/*
 * To-Do TUI
 * Copyright (C) 2023 Cameron Himes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>    // needed for text IO
#include "main.hpp"    // needed for main declarations
#include "Wrapper.hpp" // needed to call todo-cli's API
#include "TUI.hpp"     // needed for TUI

int main(int argc, char **argv) {
    // make container for filepath
    std::string filepath;

    // parse arguments
    if (argc == 2) {
        filepath = argv[1];
    }
    else if (argc > 2) {
        std::cerr << "Usage: todo-tui [FILEPATH]" << std::endl;
        return 1;
    }

    // initialize wrapper
    Wrapper wrapper = filepath.empty() ? Wrapper() : Wrapper(filepath);

    // initialize TUI
    TUI *tui = new TUI(wrapper);

    // show tables
    tui->ShowTableMenu();

    // deallocate TUI
    delete tui;

    // exit program
    return 0;
}
