/*
 * To-Do TUI
 * Copyright (C) 2023 Cameron Himes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>            // needed for text IO
#include "TerminalManager.hpp" // needed for terminal management
#include "TUI.hpp"             // needed for tui.cpp's declarations
#include "config.hpp"          // needed for user settings

TUI::TUI(Wrapper &wrapper) {
    this->wrapper = &wrapper;
}

void TUI::DrawMessageInWalls(const std::string &text) {
    size_t cols = TerminalManager::getTerminalColumns();
    size_t pos = 0;
    while (pos < text.length()) {
        // skip preceding whitespace
        while (text.at(pos) == ' ') {
            pos++;
        }
        // populate buffer with words
        std::string buffer;
        while (true) {
            // if at the end, stop running
            if (pos == text.size()) {
                break;
            }
            // check if beginning is a space
            size_t cursor = pos;
            if (text.at(cursor) == ' ') {
                // if a space is hit, check if it can be added
                if (buffer.size() < (cols-4)) {
                    buffer += ' ';
                    pos++;
                    continue;
                }
                // if it can't be added, stop and wait for next iteration
                break;
            }
            // seek next word
            while (cursor < text.size() && text.at(cursor) != ' ') {
                cursor++;
            }
            // check if word can be added
            if ((buffer.size() + (cursor - pos)) <= (cols-4)) {
                buffer += text.substr(pos, cursor-pos);
                pos = cursor;
            } else {
                // word can't be added yet, wait for next iteration
                break;
            }
        }
        // fill empty chars with spaces
        while (buffer.length() < cols-4) {
            buffer += " ";
        }
        // write out text line
        std::cout << "# " << buffer << " #" << std::endl;
    }
}

void TUI::DrawMessageBox(const std::string &text) {
    // clear terminal
    TerminalManager::clear();
    // get terminal width
    int cols = TerminalManager::getTerminalColumns();
    // draw top line
    for (int i = 0; i < cols; i++) {
        std::cout << "#";
    }
    std::cout << std::endl;
    // render text
    DrawMessageInWalls(text);
    // draw blank line
    for (int i = 0; i < cols; i++) {
        std::cout << (i == 0 || i == cols-1 ? "#" : " ");
    }
    std::cout << std::endl;
    // draw prompt text
    DrawMessageInWalls("<Press any key to continue>");
    // draw bottom bar
    for (int i = 0; i < cols; i++) {
        std::cout << "#";
    }
    std::cout << std::endl;
    // wait for keypress
    while (TerminalManager::readKey().length == 0) {}
}

std::string TUI::prompt(const std::string &text) {
    // make container for response
    std::string response;
    // show prompt
    std::cout << text << ": " << std::flush;
    // enter typing loop
    while (true) {
        // grab key
        TerminalManager::keycode_t key = TerminalManager::readKey();
        if (key.length > 0) {
            // CASE: key is normal text
            if (!iscntrl(key.keycode.at(0))) {
                response += key.keycode.at(0);
                std::cout << key.keycode.at(0) << std::flush;
            }
            // CASE: key is ENTER
            else if (TerminalManager::matchKey(key, TerminalManager::KEY_RETURN)) {
                return response;
            }
            // CASE:: key is BACKSPACE
            else if (TerminalManager::matchKey(key, TerminalManager::KEY_DELETE)) {
                if (!response.empty()) {
                    response = response.substr(0,response.length()-1);
                    std::cout << "\b \b" << std::flush;
                }
            }
        }
    }
}

void TUI::editLabels(const std::string &table) {
    // get terminal size
    size_t rows = TerminalManager::getTerminalRows();
    // ensure teminal is at least seven rows in size
    if (rows < 7) {
        DrawMessageBox("Can not display edit menu. Terminal size is too small.");
        return;
    }
    // enter menu loop
    size_t selector = 0;
    while (true) {
        // get current labels
        bool error;
        std::string explanation;
        table_t data = this->wrapper->getTable(error, explanation, table);
        if (error) {
            DrawMessageBox("Could not get table information. Reason: " + explanation);
            return;
        }
        // draw menu
        TerminalManager::clear();
        std::cout << "Editing labels for table \"" << table << "\"" << std::endl;
        std::cout
            << (selector == 0 ? TerminalManager::SEQUENCE_COLOR_INVERT : "")
            << "[Edit NAME label (currently \"" + data.name_label + "\")]"
            << (selector == 0 ? TerminalManager::SEQUENCE_COLOR_RESET : "")
            << std::endl;
        std::cout
            << (selector == 1 ? TerminalManager::SEQUENCE_COLOR_INVERT : "")
            << "[Edit LOCATION label (currently \"" + data.location_label + "\")]"
            << (selector == 1 ? TerminalManager::SEQUENCE_COLOR_RESET : "")
            << std::endl;
        std::cout
            << (selector == 2 ? TerminalManager::SEQUENCE_COLOR_INVERT : "")
            << "[Edit DATE label (currently \"" + data.date_label + "\")]"
            << (selector == 2 ? TerminalManager::SEQUENCE_COLOR_RESET : "")
            << std::endl;
        std::cout
            << (selector == 3 ? TerminalManager::SEQUENCE_COLOR_INVERT : "")
            << "[Edit DONE label (currently \"" + data.done_label + "\")]"
            << (selector == 3 ? TerminalManager::SEQUENCE_COLOR_RESET : "")
            << std::endl;
        std::cout
            << (selector == 4 ? TerminalManager::SEQUENCE_COLOR_INVERT : "")
            << "[Back]"
            << (selector == 4 ? TerminalManager::SEQUENCE_COLOR_RESET : "")
            << std::endl;
        // process response
        TerminalManager::keycode_t key = TerminalManager::readKey();
        // ARROW DOWN = move selector down
        if (TerminalManager::matchKey(key, TerminalManager::KEY_ARROW_DOWN)) {
            if (selector < 4) {
                selector++;
            }
        }
        // ARROW UP = move selector up
        else if (TerminalManager::matchKey(key, TerminalManager::KEY_ARROW_UP)) {
            if (selector > 0) {
                selector--;
            }
        }
        // ESCAPE = return to table menu
        else if (TerminalManager::matchKey(key, TerminalManager::KEY_ESCAPE)) {
            break;
        }
        // ENTER = click button
        else if (TerminalManager::matchKey(key, TerminalManager::KEY_RETURN)) {
            // CASE: edit NAME
            if (selector == 0) {
                TerminalManager::clear();
                std::string name = prompt("Enter new NAME label");
                this->wrapper->editLabel(error, explanation, table, selector_t::NAME, name);
                if (error) {
                    DrawMessageBox("Could not edit NAME label. Reason: " + explanation);
                }
            }
            // CASE: edit LOCATION
            else if (selector == 1) {
                TerminalManager::clear();
                std::string location = prompt("Enter new LOCATION label");
                this->wrapper->editLabel(error, explanation, table, selector_t::LOCATION, location);
                if (error) {
                    DrawMessageBox("Could not edit LOCATION label. Reason: " + explanation);
                }
            }
            // CASE: edit DATE
            else if (selector == 2) {
                TerminalManager::clear();
                std::string date = prompt("Enter new DATE label");
                this->wrapper->editLabel(error, explanation, table, selector_t::DATE, date);
                if (error) {
                    DrawMessageBox("Could not edit DATE label. Reason: " + explanation);
                }
            }
            // CASE: edit DONE
            else if (selector == 3) {
                TerminalManager::clear();
                std::string done = prompt("Enter new DONE label");
                this->wrapper->editLabel(error, explanation, table, selector_t::DONE, done);
                if (error) {
                    DrawMessageBox("Could not edit DONE label. Reason: " + explanation);
                }
            }
            // CASE: return to table menu
            else if (selector == 4) {
                break;
            }
        }
    }
}

void TUI::ShowTableMenu() {
    // make container for wrapper information
    bool error;
    std::string explanation;
    // show tables
    size_t selected = 0;
    size_t offset = 0;
    while (true) {
        // get a list of tables
        std::vector<std::string> tables = this->wrapper->getTables(error, explanation);
        if (error) {
            DrawMessageBox("Could not load the table list. Reason: " + explanation);
            TerminalManager::crashNormal("Fatal error from todo-cli.");
        }
        // get terminal size
        size_t rows = TerminalManager::getTerminalRows();
        // ensure teminal is at least three rows in size
        if (rows < 3) {
            TerminalManager::crashNormal("Terminal size is too small");
        }
        // print table
        TerminalManager::clear();
        std::cout << "TABLES:" << std::endl;
        if (tables.empty()) {
            std::cout << "(no tables, press a to add one)" << std::endl;
        } else {
            for (size_t i = 0; i < rows-2 && i+offset < tables.size(); i++) {
                std::cout
                    << (selected == i+offset ? TerminalManager::SEQUENCE_COLOR_INVERT : "")
                    << tables.at(i+offset)
                    << (selected == i+offset ? TerminalManager::SEQUENCE_COLOR_RESET : "")
                    << std::endl;
            }
        }
        // process keypresses
        TerminalManager::keycode_t key = TerminalManager::readKey();
        if (TerminalManager::matchKey(key, TerminalManager::KEY_ARROW_DOWN)) {
            // scroll down
            if (selected < tables.size()-1) {
                selected++;
            }
            if (selected > offset+rows-3) {
                offset++;
            }
        }
        else if (TerminalManager::matchKey(key, TerminalManager::KEY_ARROW_UP)) {
            // scroll up
            if (selected > 0) {
                selected--;
            }
            if (selected < offset) {
                offset--;
            }
        }
        else if (TerminalManager::matchKey(key, TerminalManager::KEY_RETURN)) {
            // show task menu
            if (tables.empty()) {
                continue;
            }
            ShowTaskMenu(tables.at(selected));
        }
        else if (TerminalManager::matchKey(key, "a")) {
            // add a table
            TerminalManager::clear();
            std::string name = prompt("Enter table name");
            this->wrapper->addTable(error, explanation, name);
            if (error) {
                DrawMessageBox("Table could not be added. Reason: " + explanation);
            }
        }
        else if (TerminalManager::matchKey(key, "d") || TerminalManager::matchKey(key, TerminalManager::KEY_ERASE)) {
            // delete selected table
            if (tables.empty()) {
                continue;
            }
            TerminalManager::clear();
            std::string table = tables.at(selected);
            std::cout << "Are you sure you want to delete \"" + table + "\" (y/n)? " << std::flush;
            while (true) {
                TerminalManager::keycode_t key = TerminalManager::readKey();
                if (TerminalManager::matchKey(key, "y")) {
                    this->wrapper->deleteTable(error, explanation, table);
                    if (error) {
                        DrawMessageBox("Table could not be deleted. Reason: " + explanation);
                    } else {
                        if (selected > 0) {
                            selected--;
                        }
                        if (offset > 0) {
                            offset--;
                        }
                    }
                    break;
                }
                if (TerminalManager::matchKey(key, "n")) {
                    break;
                }
            }
        }
        else if (TerminalManager::matchKey(key, "e")) {
            // edit selected table
            if (tables.empty()) {
                continue;
            }
            editLabels(tables.at(selected));
        }
        else if (TerminalManager::matchKey(key, TerminalManager::KEY_ESCAPE)) {
            // exit menu
            break;
        }
        else {
            // show keypress warning
            if (key.length > 0 && SHOW_UNKNOWN_KEYCODES) {
                std::string text = "Unknown sequence: ";
                for (size_t i = 0; i < key.length; i++) {
                    text += std::to_string((int)key.keycode.at(i)) + " ";
                }
                DrawMessageBox(text);
            }
        }
    }
}

padding_t TUI::computePadding(const table_t &table) {
    // keep track of padding
    padding_t padding;
    // consider config.hpp's stuff
    static const size_t lenDoneFalse = ((std::string)TASK_DONE_TEXT_FALSE).length();
    static const size_t lenDoneTrue = ((std::string)TASK_DONE_TEXT_TRUE).length();
    static const size_t defaultPaddingDone = lenDoneTrue > lenDoneFalse ? lenDoneTrue : lenDoneFalse;
    padding.done = defaultPaddingDone;
    // consider labels as default sizes
    padding.name     = table.name_label.length();
    padding.location = table.location_label.length();
    padding.date     = table.date_label.length();
    if (table.done_label.size() > padding.done) {
        padding.done = table.done_label.size();
    }
    // consider each task and its fields
    for (const task_t &t : table.tasks) {
        if (t.name.length() > padding.name) {
            padding.name = t.name.length();
        }
        if (t.location.length() > padding.location) {
            padding.location = t.location.length();
        }
        if (t.date.length() > padding.date) {
            padding.date = t.date.length();
        }
    }
    // return padding information
    return padding;
}

std::string TUI::padText(const std::string &text, size_t size) {
    // copy text string
    std::string ret = text;
    // keep padding until size is reached
    while (ret.length() < size) {
        ret += " ";
    }
    return ret;
}

void TUI::editTask(const std::string &table, const std::string &task) {
    // get terminal size
    size_t rows = TerminalManager::getTerminalRows();
    // ensure teminal is at least seven rows in size
    if (rows < 7) {
        DrawMessageBox("Can not display edit menu. Terminal size is too small.");
        return;
    }
    // make container for task hash
    std::string hash = task;
    // enter menu loop
    size_t selector = 0;
    while (true) {
        // refresh table data
        bool error;
        std::string explanation;
        table_t table_data = this->wrapper->getTable(error, explanation, table);
        if (error) {
            DrawMessageBox("Could not get table information. Reason: " + explanation);
            return;
        }
        // grab task
        task_t task_data;
        for (task_t task : table_data.tasks) {
            if (task.hash == hash) {
                task_data = task;
            }
        }
        // draw menu
        TerminalManager::clear();
        std::cout << "Editing fields for task id \"" << hash << "\"" << std::endl;
        std::cout
            << (selector == 0 ? TerminalManager::SEQUENCE_COLOR_INVERT : "")
            << "[Edit " + table_data.name_label + " field (currently \"" + task_data.name + "\")]"
            << (selector == 0 ? TerminalManager::SEQUENCE_COLOR_RESET : "")
            << std::endl;
        std::cout
            << (selector == 1 ? TerminalManager::SEQUENCE_COLOR_INVERT : "")
            << "[Edit " + table_data.location_label + " field (currently \"" + task_data.location + "\")]"
            << (selector == 1 ? TerminalManager::SEQUENCE_COLOR_RESET : "")
            << std::endl;
        std::cout
            << (selector == 2 ? TerminalManager::SEQUENCE_COLOR_INVERT : "")
            << "[Edit " + table_data.date_label + " field (currently \"" + task_data.date + "\")]"
            << (selector == 2 ? TerminalManager::SEQUENCE_COLOR_RESET : "")
            << std::endl;
        std::cout
            << (selector == 3 ? TerminalManager::SEQUENCE_COLOR_INVERT : "")
            << "[Toggle " + table_data.done_label + " field (currently \"" << (task_data.done ? "true" : "false") << "\")]"
            << (selector == 3 ? TerminalManager::SEQUENCE_COLOR_RESET : "")
            << std::endl;
        std::cout
            << (selector == 4 ? TerminalManager::SEQUENCE_COLOR_INVERT : "")
            << "[Back]"
            << (selector == 4 ? TerminalManager::SEQUENCE_COLOR_RESET : "")
            << std::endl;
        // process response
        TerminalManager::keycode_t key = TerminalManager::readKey();
        // ARROW DOWN = move selector down
        if (TerminalManager::matchKey(key, TerminalManager::KEY_ARROW_DOWN)) {
            if (selector < 4) {
                selector++;
            }
        }
        // ARROW UP = move selector up
        else if (TerminalManager::matchKey(key, TerminalManager::KEY_ARROW_UP)) {
            if (selector > 0) {
                selector--;
            }
        }
        // ESCAPE = return to table menu
        else if (TerminalManager::matchKey(key, TerminalManager::KEY_ESCAPE)) {
            break;
        }
        // ENTER = click button
        else if (TerminalManager::matchKey(key, TerminalManager::KEY_RETURN)) {
            // CASE: edit NAME
            if (selector == 0) {
                TerminalManager::clear();
                std::string name = prompt("Enter new " + table_data.name_label + " field");
                hash = this->wrapper->editTask(error, explanation, table, hash, selector_t::NAME, name);
                if (error) {
                    DrawMessageBox("Could not edit " + table_data.name_label + " field. Reason: " + explanation);
                }
            }
            // CASE: edit LOCATION
            else if (selector == 1) {
                TerminalManager::clear();
                std::string location = prompt("Enter new " + table_data.location_label + " field");
                hash = this->wrapper->editTask(error, explanation, table, hash, selector_t::LOCATION, location);
                if (error) {
                    DrawMessageBox("Could not edit " + table_data.location_label + " field. Reason: " + explanation);
                }
            }
            // CASE: edit DATE
            else if (selector == 2) {
                TerminalManager::clear();
                std::string date = prompt("Enter new " + table_data.date_label + " field");
                hash = this->wrapper->editTask(error, explanation, table, hash, selector_t::DATE, date);
                if (error) {
                    DrawMessageBox("Could not edit " + table_data.date_label + " field. Reason: " + explanation);
                }
            }
            // CASE: edit DONE
            else if (selector == 3) {
                hash = this->wrapper->editTask(
                    error,
                    explanation,
                    table,
                    hash,
                    selector_t::DONE,
                    !task_data.done ? "TRUE" : "FALSE"
                );
                if (error) {
                    DrawMessageBox("Could not edit " + table_data.done_label + " field. Reason: " + explanation);
                }
            }
            // CASE: return to table menu
            else if (selector == 4) {
                break;
            }
        }
    }
}

void TUI::ShowTaskMenu(const std::string &table) {
    // make container for wrapper information
    bool error;
    std::string explanation;
    // get spacing for task columns
    static const std::string spacing(TASK_COLUMN_SPACING, ' ');
    // show tasks
    size_t selected = 0;
    size_t offset = 0;
    while (true) {
        // get table data
        table_t data = this->wrapper->getTable(error, explanation, table);
        if (error) {
            DrawMessageBox("Could not load the task list. Reason: " + explanation);
            TerminalManager::crashNormal("Fatal error from todo-cli.");
        }
        padding_t padding = computePadding(data);
        // get terminal size
        size_t rows = TerminalManager::getTerminalRows();
        // ensure teminal is at least three rows in size
        if (rows < 3) {
            TerminalManager::crashNormal("Terminal size is too small");
        }
        // print table
        TerminalManager::clear();
        std::cout
            << padText(data.done_label, padding.done) << spacing
            << padText(data.date_label, padding.date) << spacing
            << padText(data.location_label, padding.location) << spacing
            << padText(data.name_label, padding.name)
            << std::endl;
        if (data.tasks.empty()) {
            std::cout << "(no tasks, press a to add one)" << std::endl;
        } else {
            for (size_t i = 0; i < rows-2 && i+offset < data.tasks.size(); i++) {
                task_t task = data.tasks.at(i+offset);
                std::cout
                    << (selected == i+offset ? TerminalManager::SEQUENCE_COLOR_INVERT : "")
                    << padText(task.done ? "[X]" : "[ ]", padding.done) << spacing
                    << padText(task.date, padding.date) << spacing
                    << padText(task.location, padding.location) << spacing
                    << padText(task.name, padding.name)
                    << (selected == i+offset ? TerminalManager::SEQUENCE_COLOR_RESET : "")
                    << std::endl;
            }
        }
        // process keypresses
        TerminalManager::keycode_t key = TerminalManager::readKey();
        if (TerminalManager::matchKey(key, TerminalManager::KEY_ARROW_DOWN)) {
            // scroll down
            if (selected < data.tasks.size()-1) {
                selected++;
            }
            if (selected > offset+rows-3) {
                offset++;
            }
        }
        else if (TerminalManager::matchKey(key, TerminalManager::KEY_ARROW_UP)) {
            // scroll up
            if (selected > 0) {
                selected--;
            }
            if (selected < offset) {
                offset--;
            }
        }
        else if (TerminalManager::matchKey(key, "a")) {
            // add a task
            TerminalManager::clear();
            std::cout << "Adding task to " << table << std::endl;
            task_t to_add;
            to_add.name = prompt("Enter task name");
            std::cout << std::endl;
            to_add.location = prompt("Enter task location");
            std::cout << std::endl;
            to_add.date = prompt("Enter task date");
            std::cout << std::endl;
            to_add.done = false;
            to_add.hash = "";
            this->wrapper->addTask(error, explanation, table, to_add);
            if (error) {
                DrawMessageBox("Task could not be added. Reason: " + explanation);
            }
        }
        else if (TerminalManager::matchKey(key, "d") || TerminalManager::matchKey(key, TerminalManager::KEY_ERASE)) {
            // delete a task
            if (data.tasks.empty()) {
                continue;
            }
            TerminalManager::clear();
            task_t to_delete = data.tasks.at(selected);
            std::cout << "Are you sure you want to delete \"" + to_delete.name + "\" (y/n)? " << std::flush;
            while (true) {
                TerminalManager::keycode_t key = TerminalManager::readKey();
                if (TerminalManager::matchKey(key, "y")) {
                    this->wrapper->deleteTask(error, explanation, table, to_delete.hash);
                    if (error) {
                        DrawMessageBox("Task could not be deleted. Reason: " + explanation);
                    } else {
                        if (selected > 0) {
                            selected--;
                        }
                        if (offset > 0) {
                            offset--;
                        }
                    }
                    break;
                }
                if (TerminalManager::matchKey(key, "n")) {
                    break;
                }
            }
        }
        else if (TerminalManager::matchKey(key, TerminalManager::KEY_TAB)) {
            // toggle done for selected task
            if (data.tasks.empty()) {
                continue;
            }
            task_t to_edit = data.tasks.at(selected);
            this->wrapper->editTask(
                error,
                explanation,
                table,
                to_edit.hash,
                selector_t::DONE,
                !to_edit.done ? "TRUE" : "FALSE"
            );
            if (error) {
                DrawMessageBox("Task could not be edited. Reason: " + explanation);
            }
        }
        else if (TerminalManager::matchKey(key, "e") || TerminalManager::matchKey(key, TerminalManager::KEY_RETURN)) {
            // edit selected task
            if (data.tasks.empty()) {
                continue;
            }
            editTask(table, data.tasks.at(selected).hash);
        }
        else if (TerminalManager::matchKey(key, TerminalManager::KEY_ESCAPE)) {
            // exit menu
            break;
        }
        else {
            // show keypress warning
            if (key.length > 0 && SHOW_UNKNOWN_KEYCODES) {
                std::string text = "Unknown sequence: ";
                for (size_t i = 0; i < key.length; i++) {
                    text += std::to_string((int)key.keycode.at(i)) + " ";
                }
                DrawMessageBox(text);
            }
        }
    }
}
