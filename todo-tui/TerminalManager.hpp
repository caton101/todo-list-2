/*
 * To-Do TUI
 * Copyright (C) 2023 Cameron Himes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TERMINALMANAGER_HPP
#define TERMINALMANAGER_HPP

//includes
#include <array>      // needed for array type
#include <string>     // needed for string type
#include <termios.h>  // needed for terminal raw mode
#include "config.hpp" // needed to use custom settings

/*
 * This namespace tracks and manages the terminal's state.
 */
namespace TerminalManager {
    extern struct termios orig_termios; // stores original terminal configuration
    extern bool is_raw_mode;            // stores terminal state

    // terminal escape sequences
    const std::string SEQUENCE_COLOR_INVERT = "\e[7m"; // flip foreground and background colors
    const std::string SEQUENCE_COLOR_RESET  = "\e[0m"; // reset all color attributes
    const std::string SEQUENCE_CLEAR        = "\e[2J"; // clear the terminal
    const std::string SEQUENCE_HOME         = "\e[H";  // reset cursor to top left
    // terminal keys
    const std::string KEY_ESCAPE       = "\e";    // ESC
    const std::string KEY_RETURN       = "\r";    // RETURN (ENTER for raw)
    const std::string KEY_DELETE       = {127};   // DELETE (BACKSPACE for raw)
    const std::string KEY_ERASE        = "\e[3~"; // ERASE (DELETE for raw)
    const std::string KEY_TAB          = "\t";    // TAB
    const std::string KEY_ARROW_UP     = "\e[A";  // UP ARROW
    const std::string KEY_ARROW_DOWN   = "\e[B";  // DOWN ARROW
    const std::string KEY_ARROW_LEFT   = "\e[D";  // LEFT ARROW
    const std::string KEY_ARROW_RIGHT  = "\e[C";  // RIGHT ARROW

    // struct for keyboard presses
    using keycode_t = struct keycode {
        std::array<char,KEYCODE_BUFFER> keycode; // keycode sequence
        size_t length;                           // length of keycode sequence
    };

    /**
     * Crash the program, using raw output.
     *
     * @param text: error message
     */
    void crashRawMode(const std::string &text);

    /**
     * Exit raw mode, returning to original "cooked" configuration.
     */
    void disableRawMode();

    /**
     * Enter raw mode, saving original configuration.
     */
    void enableRawMode();

    /**
     * Clear the terminal.
     */
    void clear();

    /**
     * Read a keycode sequence from the terminal.
     *
     * @returns a keycode sequence
     */
    keycode_t readKey();

    /**
     * Check if a keycode matches a string.
     *
     * @param keycode: the keycode
     * @param str:     the string
     * @returns        true if the keycode matches the string
     */
    bool matchKey(const keycode_t &keycode, const std::string &str);

    /**
     * Crash the program, detecting raw or cooked mode for output.
     */
    void crashNormal(const std::string &text);

    /**
     * Get number of terminal columns.
     *
     * @returns number of columns in the terminal
     */
    int getTerminalColumns();

    /**
     * Get number of terminal rows.
     *
     * @returns number of rows in the terminal
     */
    int getTerminalRows();
}

#endif // end of TERMINALMANAGER_HPP
