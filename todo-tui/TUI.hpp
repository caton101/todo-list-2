/*
 * To-Do TUI
 * Copyright (C) 2023 Cameron Himes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TUI_HPP
#define TUI_HPP

//includes
#include "Wrapper.hpp" // needed to call todo-cli's API

// make a struct for padding information
using padding_t = struct padding_t {
    size_t name;     // number of spaces to pad the NAME field
    size_t location; // number of spaces to pad the LOCATION field
    size_t date;     // number of spaces to pad the DATE field
    size_t done;     // number of spaces to pad the DONE field
};

/**
 * This class contains all TUI widgets and state information.
 */
class TUI {
    private:
        // stores the wrapper to todo-cli's API
        Wrapper *wrapper;

        /**
         * Helper for DrawMessageBox, used to print a string inside walls
         *
         * @param text: message
         */
        void DrawMessageInWalls(const std::string &text);

        /**
         * Draw a message box on the screen.
         *
         * @param text: message
         */
        void DrawMessageBox(const std::string &text);

        /**
         * Prompt the user to enter something.
         *
         * @param text: message
         * @returns     user's response
         */
        std::string prompt(const std::string &text);

        /**
         * Edit a table's labels.
         *
         * @param table: table name
         */
        void editLabels(const std::string &table);

        /**
         * Compute padding information for a table.
         *
         * @param table: the table data
         * @returns      padding information for the specified table
         */
        padding_t computePadding(const table_t &table);

        /**
         * Pad text to a specified number of characters.
         *
         * @param text: the original text
         * @param size: the new size
         * @returns     padded text
         */
        std::string padText(const std::string &text, size_t size);

        /**
         * Edit a task's fields.
         *
         * @param table: table name
         * @param task: task data
         */
        void editTask(const std::string &table, const std::string &task);
    public:
        /**
         * Initialize the TUI.
         *
         * @param wrapper: the API wrapper for todo-cli
         */
        TUI(Wrapper &wrapper);

        /**
         * Show a menu of tables to the user.
         */
        void ShowTableMenu();

        /**
         * Show a table's tasks to the user.
         *
         * @param table: table to show
         */
        void ShowTaskMenu(const std::string &table);
};

#endif // end of TUI_HPP
