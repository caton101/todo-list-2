/*
 * To-Do TUI
 * Copyright (C) 2023 Cameron Himes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "Wrapper.hpp"                           // needed for Wrapper declarations
#include "TerminalManager.hpp"                   // needed for crashing
#include "include/cpp-subprocess/subprocess.hpp" // needed to parse JSON
#include "include/json/json.hpp"                 // needed to call todo-cli

Wrapper::Wrapper() {
    // set file path to nothing and move on
    this->filepath = "";
    // check if CLI tool is in $PATH
    if (!isCLIPresent()) {
        TerminalManager::crashNormal("ERROR: Could not find todo-cli in $PATH!");
    }
}

Wrapper::Wrapper(const std::string &filepath) {
    // set the file path
    this->filepath = filepath;
    // check if CLI tool is in $PATH
    if (!isCLIPresent()) {
        TerminalManager::crashNormal("ERROR: Could not find todo-cli in $PATH!");
    }
    // check if filepath is valid
    if (!isPathValid()) {
        TerminalManager::crashNormal("ERROR: todo-cli could not open the database specified");
    }
}

bool Wrapper::isCLIPresent() {
    // COMMAND: todo-cli --api --version
    subprocess::Popen cli = subprocess::Popen(
        {"todo-cli", "--api", "--version"},
        subprocess::output{subprocess::PIPE}
    );
    nlohmann::json jdata = nlohmann::json::parse(cli.communicate().first.buf);
    return jdata["success"];
}

bool Wrapper::isPathValid() {
    // CASE 1: filepath is the default which does not need checking
    if (this->filepath.empty()) {
        return true;
    }
    // CASE 2: custom filepath is set and therefore needs checking
    // COMMAND: todo-cli --api --database FILE --version
    subprocess::Popen cli = subprocess::Popen(
        {"todo-cli", "--api", "--database", this->filepath, "--version"},
        subprocess::output{subprocess::PIPE}
    );
    nlohmann::json jdata = nlohmann::json::parse(cli.communicate().first.buf);
    return jdata["success"];
}

std::vector<std::string> Wrapper::getTables(bool &error, std::string &explanation) {
    // COMMAND: todo-cli --api --database FILE --tables
    std::vector<std::string> args = {"todo-cli", "--api"};
    if (!this->filepath.empty()) {
        args.push_back("--database");
        args.push_back(this->filepath);
    }
    args.push_back("--tables");
    subprocess::Popen cli = subprocess::Popen(
        args,
        subprocess::output{subprocess::PIPE}
    );
    nlohmann::json jdata = nlohmann::json::parse(cli.communicate().first.buf);
    error = !jdata["success"];
    explanation = jdata["explanation"];
    if (error) {
        return {};
    }
    return jdata["data"];
}

void Wrapper::addTable(bool &error, std::string &explanation, const std::string &table) {
    // COMMAND: todo-cli --api --database FILE --add-table TABlE
    std::vector<std::string> args = {"todo-cli", "--api"};
    if (!this->filepath.empty()) {
        args.push_back("--database");
        args.push_back(this->filepath);
    }
    args.push_back("--add-table");
    args.push_back(table);
    subprocess::Popen cli = subprocess::Popen(
        args,
        subprocess::output{subprocess::PIPE}
    );
    nlohmann::json jdata = nlohmann::json::parse(cli.communicate().first.buf);
    error = !jdata["success"];
    explanation = jdata["explanation"];
}

void Wrapper::deleteTable(bool &error, std::string &explanation, const std::string &table) {
    // COMMAND: todo-cli --api --database FILE --delete-table TABLE
    std::vector<std::string> args = {"todo-cli", "--api"};
    if (!this->filepath.empty()) {
        args.push_back("--database");
        args.push_back(this->filepath);
    }
    args.push_back("--delete-table");
    args.push_back(table);
    subprocess::Popen cli = subprocess::Popen(
        args,
        subprocess::output{subprocess::PIPE}
    );
    nlohmann::json jdata = nlohmann::json::parse(cli.communicate().first.buf);
    error = !jdata["success"];
    explanation = jdata["explanation"];
}

void Wrapper::renameTable(bool &error, std::string &explanation, const std::string &oldName, const std::string &newName) {
    // COMMAND: todo-cli --api --database FILE --rename-table OLD NEW
    std::vector<std::string> args = {"todo-cli", "--api"};
    if (!this->filepath.empty()) {
        args.push_back("--database");
        args.push_back(this->filepath);
    }
    args.push_back("--rename-table");
    args.push_back(oldName);
    args.push_back(newName);
    subprocess::Popen cli = subprocess::Popen(
        args,
        subprocess::output{subprocess::PIPE}
    );
    nlohmann::json jdata = nlohmann::json::parse(cli.communicate().first.buf);
    error = !jdata["success"];
    explanation = jdata["explanation"];
}

void Wrapper::editLabel(bool &error, std::string &explanation, const std::string &table, selector_t selector, const std::string &newLabel) {
    // COMMAND: todo-cli --api --database FILE --edit-label TABLE SELECTOR LABEL
    std::vector<std::string> args = {"todo-cli", "--api"};
    if (!this->filepath.empty()) {
        args.push_back("--database");
        args.push_back(this->filepath);
    }
    args.push_back("--edit-label");
    args.push_back(table);
    switch (selector) {
        case selector_t::NAME:
            args.push_back("NAME");
            break;
        case selector_t::LOCATION:
            args.push_back("LOCATION");
            break;
        case selector_t::DATE:
            args.push_back("DATE");
            break;
        case selector_t::DONE:
            args.push_back("DONE");
            break;
    }
    args.push_back(newLabel);
    subprocess::Popen cli = subprocess::Popen(
        args,
        subprocess::output{subprocess::PIPE}
    );
    nlohmann::json jdata = nlohmann::json::parse(cli.communicate().first.buf);
    error = !jdata["success"];
    explanation = jdata["explanation"];
}

table_t Wrapper::getTable(bool &error, std::string &explanation, const std::string &table) {
    // COMMAND: todo-cli --api --database FILE --tasks TABLE
    std::vector<std::string> args = {"todo-cli", "--api"};
    if (!this->filepath.empty()) {
        args.push_back("--database");
        args.push_back(this->filepath);
    }
    args.push_back("--tasks");
    args.push_back(table);
    subprocess::Popen cli = subprocess::Popen(
        args,
        subprocess::output{subprocess::PIPE}
    );
    nlohmann::json jdata = nlohmann::json::parse(cli.communicate().first.buf);
    error = !jdata["success"];
    explanation = jdata["explanation"];
    table_t table_data;
    if (error) {
        return table_data;
    }
    table_data.name_label = jdata["data"][0]["name"];
    table_data.location_label = jdata["data"][0]["location"];
    table_data.date_label = jdata["data"][0]["date"];
    table_data.done_label = jdata["data"][0]["done"];
    table_data.hash_label = jdata["data"][0]["hash"];
    for (size_t i = 1; i < jdata["length"]; i++) {
        task_t task;
        task.name = jdata["data"][i]["name"];
        task.location = jdata["data"][i]["location"];
        task.date = jdata["data"][i]["date"];
        task.done = jdata["data"][i]["done"];
        task.hash = jdata["data"][i]["hash"];
        table_data.tasks.push_back(task);
    }
    return table_data;
}

void Wrapper::addTask(bool &error, std::string &explanation, const std::string &table, task_t task) {
    // COMMAND: todo-cli --api --database FILE --add-task TABLE NAME LOCATION DATE DONE
    std::vector<std::string> args = {"todo-cli", "--api"};
    if (!this->filepath.empty()) {
        args.push_back("--database");
        args.push_back(this->filepath);
    }
    args.push_back("--add-task");
    args.push_back(table);
    args.push_back(task.name);
    args.push_back(task.location);
    args.push_back(task.date);
    args.push_back(task.done ? "true" : "false");
    subprocess::Popen cli = subprocess::Popen(
        args,
        subprocess::output{subprocess::PIPE}
    );
    nlohmann::json jdata = nlohmann::json::parse(cli.communicate().first.buf);
    error = !jdata["success"];
    explanation = jdata["explanation"];
}

void Wrapper::deleteTask(bool &error, std::string &explanation, const std::string &table, const std::string &hash) {
    // COMMAND: todo-cli --api --database FILE --delete-task TABLE HASH
    std::vector<std::string> args = {"todo-cli", "--api"};
    if (!this->filepath.empty()) {
        args.push_back("--database");
        args.push_back(this->filepath);
    }
    args.push_back("--delete-task");
    args.push_back(table);
    args.push_back(hash);
    subprocess::Popen cli = subprocess::Popen(
        args,
        subprocess::output{subprocess::PIPE}
    );
    nlohmann::json jdata = nlohmann::json::parse(cli.communicate().first.buf);
    error = !jdata["success"];
    explanation = jdata["explanation"];
}

std::string Wrapper::editTask(bool &error, std::string &explanation, const std::string &table, const std::string &hash, selector_t selector, const std::string &value) {
    // COMMAND: todo-cli --api --database FILE --edit-task TABLE HASH SELECTOR VALUE
    std::vector<std::string> args = {"todo-cli", "--api"};
    if (!this->filepath.empty()) {
        args.push_back("--database");
        args.push_back(this->filepath);
    }
    args.push_back("--edit-task");
    args.push_back(table);
    args.push_back(hash);
    switch (selector) {
        case selector_t::NAME:
            args.push_back("NAME");
            break;
        case selector_t::LOCATION:
            args.push_back("LOCATION");
            break;
        case selector_t::DATE:
            args.push_back("DATE");
            break;
        case selector_t::DONE:
            args.push_back("DONE");
            break;
    }
    args.push_back(value);
    subprocess::Popen cli = subprocess::Popen(
        args,
        subprocess::output{subprocess::PIPE}
    );
    nlohmann::json jdata = nlohmann::json::parse(cli.communicate().first.buf);
    error = !jdata["success"];
    explanation = jdata["explanation"];
    if (error) {
        return "";
    }
    return jdata["data"][0];
}
