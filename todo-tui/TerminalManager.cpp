/*
 * To-Do TUI
 * Copyright (C) 2023 Cameron Himes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <ctype.h>             // needed for terminal raw mode
#include <errno.h>             // needed for terminal raw mode
#include <stdio.h>             // needed for terminal raw mode
#include <stdlib.h>            // needed for terminal raw mode
#include <unistd.h>            // needed for terminal raw mode
#include <sys/ioctl.h>         // needed for terminal size
#include <iostream>            // needed for text IO
#include "TerminalManager.hpp" // needed for TerminalManager.cpp's declarations

/*
 * Define the cursed globals needed for terminal state management
 *
 * NOTE: This can NOT go in the header or it will cause compilation errors.
 * See the following link for more information.
 * https://stackoverflow.com/questions/14526153/multiple-definition-of-a-global-variable
 */
namespace TerminalManager {
    struct termios orig_termios; // stores original terminal configuration
    bool is_raw_mode;            // stores terminal state
}

void TerminalManager::crashRawMode(const std::string &text) {
    // All credit for this function goes to:
    // https://viewsourcecode.org/snaptoken/kilo/02.enteringRawMode.html

    // show error message
    perror(text.c_str());
    // exit with bad status code
    exit(1);
}

void TerminalManager::disableRawMode() {
    // All credit for this function goes to:
    // https://viewsourcecode.org/snaptoken/kilo/02.enteringRawMode.html

    // attempt to reset terminal settings
    if (tcsetattr(STDIN_FILENO, TCSAFLUSH, &orig_termios) == -1) {
        crashRawMode("tcsetattr");
    }
    // set raw mode flag
    is_raw_mode = false;
}

void TerminalManager::enableRawMode() {
    // All credit for this function goes to:
    // https://viewsourcecode.org/snaptoken/kilo/02.enteringRawMode.html

    // save the current terminal settings
    if (tcgetattr(STDIN_FILENO, &orig_termios) == -1) {
        crashRawMode("tcgetattr");
    }

    // ensure terminal settings are restored on exit
    atexit(disableRawMode);

    // create terminal settings
    struct termios raw = orig_termios;

    // configure raw mode
    raw.c_iflag &= ~(BRKINT | ICRNL | INPCK | ISTRIP | IXON);
    raw.c_oflag &= ~(OPOST);
    raw.c_cflag |= (CS8);
    raw.c_lflag &= ~(ECHO | ICANON | IEXTEN | ISIG);

    // configure read timeouts
    raw.c_cc[VMIN] = 0;
    raw.c_cc[VTIME] = VTIME_SECONDS * 10;

    // attempt to apply terminal settings
    if (tcsetattr(STDIN_FILENO, TCSAFLUSH, &raw) == -1) {
        crashRawMode("tcsetattr");
    }

    // set raw mode flag
    is_raw_mode = true;
}

void TerminalManager::clear() {
    // reset cursor to top left and clear text
    std::cout << SEQUENCE_HOME << SEQUENCE_CLEAR << std::flush;
}

TerminalManager::keycode_t TerminalManager::readKey() {
    bool wasCooked = false;
    if (!is_raw_mode) {
        enableRawMode();
        wasCooked = true;
    }
    // make keycode struct
    keycode_t buffer;
    // read the keyboard
    int len = read(STDIN_FILENO, &buffer.keycode, KEYCODE_BUFFER);
    // check for a read error
    if (len < 0) {
        crashRawMode("read");
    }
    // set the length
    buffer.length = len;
    // restore terminal
    if (wasCooked) {
        disableRawMode();
    }
    // return the keycode
    return buffer;
}

bool TerminalManager::matchKey(const keycode_t &keycode, const std::string &str) {
    // match size
    if (keycode.length != str.length()) {
        return false;
    }
    // match chars
    for (size_t i = 0; i < str.length(); i++) {
        if (keycode.keycode.at(i) != str.at(i)) {
            return false;
        }
    }
    return true;
}

void TerminalManager::crashNormal(const std::string &text) {
    // show error message
    if (is_raw_mode) {
        std::cout << "ERROR: " << text << "\r\n";
    } else {
        std::cout << "ERROR: " << text << std::endl;
    }
    // stop program since fatal error occured
    exit(1);
}

int TerminalManager::getTerminalColumns() {
    // try to get column size from ENV
    char *colsTxt = getenv("COLUMNS");
    if (colsTxt != nullptr) {
        return atoi(colsTxt);
    }
    // use ioctl instead
    struct winsize size;
    ioctl(STDOUT_FILENO, TIOCGWINSZ, &size);
    return size.ws_col;
}

int TerminalManager::getTerminalRows() {
    // try to get row size from ENV
    char *rowsTxt = getenv("LINES");
    if (rowsTxt != nullptr) {
        return atoi(rowsTxt);
    }
    // use ioctl instead
    struct winsize size;
    ioctl(STDOUT_FILENO, TIOCGWINSZ, &size);
    return size.ws_row;
}

