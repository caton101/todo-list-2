/*
 * To-Do TUI
 * Copyright (C) 2023 Cameron Himes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

// this sets the largest number of characters in one keycode
#define KEYCODE_BUFFER 10

// this sets the timeout to wait for keys
#define VTIME_SECONDS 1

// if true, a warning will be shown for unknown keycodes
#define SHOW_UNKNOWN_KEYCODES false

// this sets the spacing between task columns
#define TASK_COLUMN_SPACING 3

// this sets the text for a task's done status
#define TASK_DONE_TEXT_TRUE "[X]"
#define TASK_DONE_TEXT_FALSE "[ ]"
