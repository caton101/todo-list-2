/*
 * To-Do TUI
 * Copyright (C) 2023 Cameron Himes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

 #ifndef WRAPPER_HPP
 #define WRAPPER_HPP

// includes
#include <string> // needed for string type
#include <vector> // needed for vector type

// struct for a task
using task_t = struct task {
    std::string name;     // name or description
    std::string location; // location or topic
    std::string date;     // date in the format YYYY-MM-DD HH:MM:SS
    std::string hash;     // hash assigned by the CLI tool
    bool done;            // completion status (true if complete)
};

// struct for storing a task table
using table_t = struct table {
    std::string name_label;     // the user-defined label for the name field
    std::string location_label; // the user-defined label for the location field
    std::string date_label;     // the user-defined label for the date field
    std::string done_label;     // the user-defined label for the done field
    std::string hash_label;     // the label of the hash field
    std::vector<task_t> tasks;  // the vector of tasks
};

// selectors for edit commands
enum selector_t {
    NAME,
    LOCATION,
    DATE,
    DONE
};

/**
 * This class wraps the todo-cli command into an API.
 */
class Wrapper {
    private:
        // the filepath to the database
        std::string filepath;

        /**
         * Check if the todo-cli command can be called.
         *
         * @returns true if the todo-cli command executes with no error
         */
        bool isCLIPresent();

        /**
         * Check if the todo-cli command can be called with a custom database.
         *
         * @returns true if the todo-cli command executes with no error
         */
        bool isPathValid();
    public:
        /**
         * Creates a Wrapper with the default database path as chosen by the
         * todo-cli command.
         */
        Wrapper();

        /**
         * Creates a Wrapper with a custom database path as chosen by the user.
         *
         * @param filepath: the file path to the database
         */
        Wrapper(const std::string &filepath);

        /**
         * Get a vector of table names.
         *
         * @param error:       true if an error occured
         * @param explanation: todo-cli's explanation of the exit status
         * @returns            a vector of table names
         */
        std::vector<std::string> getTables(
            bool &error,
            std::string &explanation
        );

        /**
         * Add a table.
         *
         * @param error:       true if an error occured
         * @param explanation: todo-cli's explanation of the exit status
         * @param table:       name of the table to add
         */
        void addTable(
            bool &error,
            std::string &explanation,
            const std::string &table
        );

        /**
         * Delete a table.
         *
         * @param error:       true if an error occured
         * @param explanation: todo-cli's explanation of the exit status
         * @param table:       name of the table to delete
         */
        void deleteTable(
            bool &error,
            std::string &explanation,
            const std::string &table
        );

        /**
         * Rename a table.
         *
         * @param error:       true if an error occured
         * @param explanation: todo-cli's explanation of the exit status
         * @param oldName:     old (current) name of the table
         * @param newName:     new name of the table
         */
        void renameTable(
            bool &error,
            std::string &explanation,
            const std::string &oldName,
            const std::string &newName
        );

        /**
         * Edit a table's label text.
         *
         * @param error:       true if an error occured
         * @param explanation: todo-cli's explanation of the exit status
         * @param table:       name of the table to edit
         * @param selector:    label to change
         * @param newLabel:    new label text
         */
        void editLabel(
            bool &error,
            std::string &explanation,
            const std::string &table,
            selector_t selector,
            const std::string &newLabel
        );

        /**
         * Get a table's labels and tasks.
         *
         * @param error:       true if an error occured
         * @param explanation: todo-cli's explanation of the exit status
         * @param table:       name of the table to get
         * @returns            specified table's labels and tasks
         */
        table_t getTable(
            bool &error,
            std::string &explanation,
            const std::string &table
        );

        /**
         * Add a task to a table.
         *
         * @param error:       true if an error occured
         * @param explanation: todo-cli's explanation of the exit status
         * @param table:       table to which the task is appended
         * @param task:        task information to add to the table
         */
        void addTask(
            bool &error,
            std::string &explanation,
            const std::string &table,
            task_t task
        );

        /**
         * Delete a task from a table.
         *
         * @param error:       true if an error occured
         * @param explanation: todo-cli's explanation of the exit status
         * @param table:       table where the task is present
         * @param hash:        hash of the task to delete
         */
        void deleteTask(
            bool &error,
            std::string &explanation,
            const std::string &table,
            const std::string &hash
        );

        /**
         * Edit a task in a table.
         *
         * @param error:       true if an error occured
         * @param explanation: todo-cli's explanation of the exit status
         * @param table:       table where the task is present
         * @param hash:        hash of the task to edit
         * @param selector:    task field to edit
         * @param value:       new value for the specified field
         * @returns            the updated hash of the task
         */
        std::string editTask(
            bool &error,
            std::string &explanation,
            const std::string &table,
            const std::string &hash,
            selector_t selector,
            const std::string &value
        );
};

#endif // end of WRAPPER_HPP
