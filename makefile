all:
	$(MAKE) todo-cli
	$(MAKE) todo-tui
	$(MAKE) man
	$(MAKE) build

todo-cli:
	$(MAKE) -C ./todo-cli todo-cli

todo-tui:
	$(MAKE) -C ./todo-tui todo-tui

man:
	$(MAKE) -C ./man all

build:
	mkdir -p build
	cp ./todo-cli/todo-cli ./build/todo-cli
	cp ./todo-tui/todo-tui ./build/todo-tui
	cp ./man/todo-cli.1.gz ./build/todo-cli.1.gz
	cp ./man/todo-tui.1.gz ./build/todo-tui.1.gz

clean:
	$(MAKE) -C ./todo-cli clean
	$(MAKE) -C ./todo-tui clean
	$(MAKE) -C ./man clean
	rm -rf ./build

install:
	chmod +x ./tools/install-global.sh
	./tools/install-global.sh

uninstall:
	chmod +x ./tools/uninstall-global.sh
	./tools/uninstall-global.sh

.PHONY: all todo-cli todo-tui man build clean install uninstall
