# To-Do List 2

This program tracks your tasks and deadlines. Unlike alternatives, this program
is designed for use in a terminal. It is lightweight and offers great
flexibility. All tasks are kept in a single database file for easy
synchronization with a third party tool like [Nextcloud][0] or [Dropbox][1].

**NOTE**: This tool is not compatible with my [first version][2].

## Building

This is a standard C++ project which will work with almost any setup.

**NOTE**: These instructions will not work on Windows.
**NOTE 2**: These instructions are only tested on Linux, but should work on Mac
OS, BSD, and any other POSIX-like operating system.

1. Clone the repository: `git clone https://gitlab.com/caton101/todo-list-2.git`
2. Enter the directory: `cd todo-list-2`
3. Build the project: `make`

The same as above, but easier to copy:

```sh
git clone https://gitlab.com/caton101/todo-list-2.git
cd todo-list-2
make
```

## Installing

1. Follow the steps in [Building](#Building)
2. Run `make install`

## Uninstalling

1. Follow steps 1 and 2 from [Building](#Building)
2. Run `make uninstall`

## To-Do CLI Documentation

This is the command line utility for managing To-Do Lists. It supports things
like timezone adjustments, multiple lists (called tables), and changing the
field labels to fit specific needs.

### Usage

The program can be executed from inside a terminal using `todo-cli [prefixes] <command> <args>` .

#### Prefixes (optional)

| Flag            | Description                   |
| :-------------- | :---------------------------- |
| --api           | print output as a JSON object |
| --database FILE | use a custom database path    |

#### Table Commands

| Flag                              | Description              |
| :-------------------------------- | :----------------------- |
| --tables                          | list all tables          |
| --edit-label TABLE SELECTOR LABEL | edit a label for a table |
| --delete-table TABLE              | delete a table           |
| --rename-table TABLE NAME         | rename a table           |
| --add-table NAME                  | add a table              |

#### Task Commands

| Flag                                     | Description               |
| :--------------------------------------- | :------------------------ |
| --tasks TABLE                            | list all tasks in a table |
| --edit-task TABLE TASK SELECTOR VALUE    | edit a task attribute     |
| --add-task TABLE NAME LOCATION DATE DONE | add a task                |
| --delete-task TABLE TASK                 | delete a task             |

#### Information Commands

| Flag               | Description           |
| :----------------- | :-------------------- |
| todo-cli --help    | show help information |
| todo-cli --usage   | show command syntax   |
| todo-cli --version | show version number   |

#### Terminology

| Term     | Description                                                 |
| :------- | :---------------------------------------------------------- |
| DATE     | the date a task occurs (in the format YYYY-MM-DD HH:MM:SS)  |
| DONE     | the completion status of a task (either true or false)      |
| FILE     | a path to a file                                            |
| LABEL    | a name used for labeling task attributes                    |
| LOCATION | the location a task occurs                                  |
| NAME     | the name of a table or task                                 |
| SELECTOR | a column position (see Selectors for possible values        |
| TABLE    | the name of a table                                         |
| TASK     | the ID for a specific task                                  |
| VALUE    | either a NAME, LOCATION, DATE, or DONE depending on context |

#### Selectors

| Term     | Description                          |
| :------- | :----------------------------------- |
| NAME     | select the task or table name column |
| LOCATION | select the location column           |
| DATE     | select the date column               |
| DONE     | select the completion status column  |

### API

To use the API feature, prepend `--api` to the command flags. This switches the
output to a JSON string. The possible outputs are described below.

#### An Error Response

Errors happen when the provided command can not be performed or an argument is
invalid. This can be seen in the following JSON data:

```json
{
    "success": false,
    "explanation": "ERROR MESSAGE HERE",
    "length": 0,
    "data": []
}
```

The easy way to parse errors is to check the `success` field and then read the
error from the `explanation` field. Do not rely on a specific explanation since
the string is subject to change.

#### An Empty Response

An empty response happens when a command does not need to output any
information, but still prints an acknowledgment. This happens with the table
modification commands. An example can be seen in the following JSON data:

```json
{
    "success": true,
    "explanation": "STATUS MESSAGE HERE",
    "length": 0,
    "data": []
}
```

The easy way to parse errors is to check the `success` field and then read the
error from the `explanation` field if needed. A successful command will have a
value of `true` inside the `success` field which reports a successful command
execution.

#### A String Array Response

An array of strings is only generated when the non-api command outputs multiple
sequential strings. This happens when using `--help`, `--tables`, `--usage`,
and `--version`. An example can be seen in the following JSON data:

```json
{
    "success": true,
    "explanation": "STATUS MESSAGE HERE",
    "length": 3,
    "data": [
        "foo",
        "bar",
        "baz"
    ]
}
```

The easy way to parse single array responses is to check the `success` field
and read the `explanation` field if there is an error. The `length` field
contains the number of strings inside the `data` field.

#### A Task List Response

A task list response is generated when the `--tasks` flag is used. It works
similarly to the string array response except `data` contains nested JSON
objects instead of strings. An example can be seen in the following JSON data:

```json
{
    "success": true,
    "explanation": "COMMAND NAME HERE",
    "length": 2,
    "data": [
        {
            "name": "myFirstName",
            "location": "myFirstLocation",
            "date": "1970-01-01 00:00:00",
            "done": false,
            "hash": "HASH_CODE_1"
        },
        {
            "name": "mySecondName",
            "location": "mySecondLocation",
            "date": "1970-01-01 00:00:00",
            "done": true,
            "hash": "HASH_CODE_2"
        }
    ]
}
```

The easy way to parse this is to check the `success` field and read the
`explanation` field if there is an error. The `length` field contains the
number of tasks in the `data` array. The **first** element contains the
user-defined field labels. These labels are always in the output even if there
are no tasks in a table.

### Programming Notes

The programming notes are broken into two sections: the structure of stored
data and a breakdown of the source code files.

#### Data Storage

This program uses SQLite 3 for storing all information. The task lists are
stored in a special table called `ATLAS`. This table contains the name of each
task list, the field labels, and the SQL name for the task table. The task
tables contain the actual task information. A breakdown of the table structure
is below:

##### ATLAS

```txt
NAME           STRING NOT NULL
TASK_LABEL     STRING NOT NULL
LOCATION_LABEL STRING NOT NULL
DATE_LABEL     STRING NOT NULL
DONE_LABEL     STRING NOT NULL
TABLE_ID       STRING NOT NULL
```

| Column         | Description                                |
| :------------- | :----------------------------------------- |
| NAME           | The user-specified name for the task table |
| TASK_LABEL     | the label for a task name                  |
| LOCATION_LABEL | the label for a task location              |
| DATE_LABEL     | the label for a task date                  |
| DONE_LABEL     | the label for the task completion status   |
| TABLE_ID       | the actual table name                      |

##### Task Tables

```txt
NAME     STRING NOT NULL
LOCATION STRING NOT NULL
DATE     STRING NOT NULL
DONE     STRING NOT NULL
HASH     STRING NOT NULL
```

| Column         | Description                      |
| :------------- | :------------------------------- |
| NAME           | a description of the task        |
| LOCATION       | where the task occurs            |
| DATE           | the time a task is due or occurs |
| DONE           | the completion status of a task  |
| HASH           | a unique identifier for the task |

#### Source Code Files

The source code files are broken down into sections based on what task is being
performed. A breakdown of the files can be found below.

| File       | Description                                                   |
| :--------- | :------------------------------------------------------------ |
| args.cpp   | the argument parser                                           |
| common.cpp | miscellaneous helper functions                                |
| driver.cpp | the command processor                                         |
| main.cpp   | the entrypoint for code                                       |
| sql.cpp    | the I/O handler                                               |
| args.h     | declarations for the argument parser                          |
| common.h   | declarations for helper functions and custom struct types     |
| config.h   | settings for advanced customization                           |
| driver.h   | declarations for command processor                            |
| main.h     | declarations for main (not needed, but included for symmetry) |
| sql.h      | decorations for the I/O handler                               |

The source code contains several design decisions which may seem strange to
outsiders. These questions should help you understand why I made specific
decisions when writing this program.

> Why is there a stringEquals command instead of using `==`?

It is more useful for things like `<std::string> == char *` which is a very
common occurrence in the code.

> The code uses ID and Hash interchangeably. Why use both?

Shockingly, this avoids confusion. A table id is very ambiguous. What does
"table id" mean? Is it the name of the table? It is a random identifier to
ensure each SQL line is unique? None of these are true. The user's perception
of a table name is just a human alias to the real table name. Both are table
id's but only one is a real id. To avoid confusion, I called the real table id
a hash since hashes are supposed to be unique and because my original code was
supposed to use a SHA hash for generating id's.

> Why is the padding function called `leftJustify` instead of `rightPad`?

I was a Python user for a long time. The equivalent function in Python is
called `leftJustify`. It made sense for me to keep the same function name.

> Why do `dateStringToLong` and `dateLongToString` both convert timezones?

I am lazy. The stored time should be a standard UNIX timestamp to ensure
timezone independence. I also know most people will give you a time in their
own timezone. This is why the `dateStringToLong` function converts local time
to global time. The same logic applies to the other one. Most people expect
their computer to provide times in their local timezone so the
`dateLongToString` function performs this conversion.

> Why do the `SQLEngine` methods store their error status in a field?

This happens because some methods need to use the return statement for actual
data. Returning the tables list and task lists would be really hard without
using a return type. To keep it consistent with the void methods, the status
was implemented in a field that can easily be retrieved at any time.

## To-Do TUI Documentation

This program is a simple TUI To-Do List which uses todo-cli(1)'s API. Everything
that can be done in the command line tool can be done from this application.

### Usage

- `todo-tui`: Open the default task database
- `todo-tui FILE`: Open FILE as the task database

### Keybinds

#### Global

| Key        | Description            |
| :--------- | :--------------------- |
| up arrow   | Move selection up      |
| down arrow | Move selection down    |
| y          | Answer yes to a prompt |
| n          | Answer no to a prompt  |

#### Table Selection Menu

| Key   | Description           |
| :---- | :-------------------- |
| a     | Add a table           |
| d     | Delete selected table |
| e     | Edit selected table   |
| del   | Delete selected table |
| enter | Show selected table   |
| esc   | Close program         |

#### Task List Menu

| Key   | Description                        |
| :---- | :--------------------------------- |
| a     | Add a task                         |
| d     | Delete selected task               |
| e     | Edit selected task                 |
| del   | Delete selected task               |
| enter | Edit selected task                 |
| esc   | Return to table menu               |
| tab   | Toggle completion of selected task |

## Credits

Most of the codebase belongs to Cameron Himes which is under the GNU General
Public License Version 3. The few exceptions are mentioned below.

JSON was parsed using [JSON][3] by Niels Lohmann which is available under the
MIT License.

Calls to todo-cli are performed using [cpp-subprocess][4] by Arun Muralidharan
which is available under the MIT License.

Code to enter and exit raw mode is provided by [Kilo][5] using [this guide][6]
by Salvatore Sanfilippo which is under the BSD 2-Clause "Simplified" License.

[0]: https://nextcloud.com/
[1]: https://www.dropbox.com/
[2]: https://gitlab.com/caton101/todo-list
[3]: https://github.com/nlohmann/json
[4]: https://github.com/arun11299/cpp-subprocess
[5]: https://github.com/snaptoken/kilo-src
[6]: https://viewsourcecode.org/snaptoken/kilo/02.enteringRawMode.html
