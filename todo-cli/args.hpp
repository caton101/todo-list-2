/*
 * To-Do CLI
 * Copyright (C) 2022 Cameron Himes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

// imports
#include <vector>
#include <string>
#include "common.hpp"

// put everything inside ARGS namespace
namespace ARGS
{
    // command definitions
    const int COMMAND_TABLES = 0;
    const int COMMAND_EDIT_LABEL = 1;
    const int COMMAND_DELETE_TABLE = 2;
    const int COMMAND_RENAME_TABLE = 3;
    const int COMMAND_ADD_TABLE = 4;
    const int COMMAND_TASKS = 5;
    const int COMMAND_EDIT_TASK = 6;
    const int COMMAND_ADD_TASK = 7;
    const int COMMAND_DELETE_TASK = 8;
    const int COMMAND_HELP = 9;
    const int COMMAND_USAGE = 10;
    const int COMMAND_VERSION = 11;
    const int COMMAND_LICENSE = 12;
    const int COMMAND_TEST = 13;
    const int COMMAND_ERROR_NO_ARGS = 14;
    const int COMMAND_ERROR_FEW_ARGS = 15;
    const int COMMAND_ERROR_EXCESS_ARGS = 16;
    const int COMMAND_ERROR_INVALID = 17;

    // function declarations
    bool offsetLegal(int argc, int offset);
    bool notEnoughArgs(int argc, int offset, int args);
    std::string commandToString(int command);
    command_data_t parseArgs(int argc, char **argv);
}
