/*
 * To-Do CLI
 * Copyright (C) 2022 Cameron Himes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

// includes
#include <string>

/**
 * @brief  This contains terminal color codes
 */
namespace colors
{
    // white foreground escape code
    const std::string white = "\033[0;97m";

    // black foreground escape code
    const std::string black = "\033[0;30m";

    // light grey foreground escape code
    const std::string light_grey = "\033[0;37m";

    // dark grey foreground escape code
    const std::string dark_grey = "\033[0;90m";

    // red foreground escape code
    const std::string red = "\033[0;31m";

    // light red foreground escape code
    const std::string light_red = "\033[0;91m";

    // orange foreground escape code
    const std::string orange = "\033[0;33m";

    // yellow foreground escape code
    const std::string yellow = "\033[0;93m";

    // green foreground escape code
    const std::string green = "\033[0;32m";

    // light green foreground escape code
    const std::string light_green = "\033[0;92m";

    // cyan foreground escape code
    const std::string cyan = "\033[0;36m";

    // light cyan foreground escape code
    const std::string light_cyan = "\033[0;96m";

    // blue foreground escape code
    const std::string blue = "\033[0;34m";

    // light blue foreground escape code
    const std::string light_blue = "\033[0;94m";

    // purple foreground escape code
    const std::string purple = "\033[0;35m";

    // magenta foreground escape code
    const std::string magenta = "\033[0;95m";

    // reset foreground escape code
    const std::string reset = "\u001b[0m";
}