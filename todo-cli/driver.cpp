/*
 * To-Do CLI
 * Copyright (C) 2022 Cameron Himes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <vector>
#include <string>
#include <iostream>
#include <cmath>
#include "driver.hpp"
#include "common.hpp"
#include "sql.hpp"
#include "args.hpp"
#include "config.hpp"
#include "colors.hpp"

void Driver::list_tables(command_data_t command)
{
    // make SQL engine
    SQL::SQLEngine sql = SQL::SQLEngine(command.database);
    // get the table names
    std::vector<std::string> tables = sql.get_task_table_names();
    // check the API mode
    if (command.api)
    {
        // print in JSON format
        std::string json = "{\"success\": ";
        json += sql.get_status() == SQL::STATUS_SUCCESS ? "true" : "false";
        json += ", \"explanation\": \"";
        json += SQL::explain_status(sql.get_status());
        json += "\", \"length\": ";
        json += std::to_string(tables.size());
        json += ", \"data\": [";
        for (std::string i : tables)
        {
            json += "\"" + i + "\", ";
        }
        if (tables.size() > 0)
        {
            // if there were tables, remove the ", " from the end of the list
            json.pop_back();
            json.pop_back();
        }
        json += "]}";
        std::cout << json << std::endl;
    }
    else
    {
        // print normally
        if (tables.size())
        {
            std::cout << "Tables:" << std::endl;
            bool even = false;
            for (std::string i : tables)
            {
                std::cout << "-> " << (even ? colors::yellow : colors::light_blue) << i << colors::reset << std::endl;
                even = !even;
            }
        }
        else
        {
            std::cout << colors::red << "There are no tables." << colors::reset << std::endl;
        }
    }
}

void Driver::edit_table(command_data_t command)
{
    // pull args out from command
    std::string table = command.args.at(0);
    std::string column = command.args.at(1);
    std::string label = command.args.at(2);
    // get the column selector
    int selector = 0;
    if (stringEquals(column, "NAME"))
    {
        selector = SQL::COLUMN_NAME;
    }
    else if (stringEquals(column, "LOCATION"))
    {
        selector = SQL::COLUMN_LOCATION;
    }
    else if (stringEquals(column, "DATE"))
    {
        selector = SQL::COLUMN_DATE;
    }
    else if (stringEquals(column, "DONE"))
    {
        selector = SQL::COLUMN_DONE;
    }
    else
    {
        generic_error(command, "Invalid column selector.");
        return;
    }
    // make SQL engine
    SQL::SQLEngine sql = SQL::SQLEngine(command.database);
    // edit the table
    sql.edit_task_table(selector, label, table);
    // check status
    int status = sql.get_status();
    // print response
    if (command.api)
    {
        // print using JSON
        std::string json = "{\"success\": ";
        json += status == SQL::STATUS_SUCCESS ? "true" : "false";
        json += ", \"explanation\": \"";
        json += SQL::explain_status(status);
        json += "\", \"length\": 0, \"data\": []}";
        std::cout << json << std::endl;
    }
    else
    {
        // print normally
        if (status == SQL::STATUS_SUCCESS)
        {
            std::cout << colors::green << "Ok, the table was updated successfully." << colors::reset << std::endl;
        }
        else
        {
            generic_error(command, SQL::explain_status(status));
        }
    }
}

void Driver::delete_table(command_data_t command)
{
    // pull table from args
    std::string table = command.args.at(0);
    // make SQL engine
    SQL::SQLEngine sql = SQL::SQLEngine(command.database);
    // delete the table
    sql.delete_task_table(table);
    // get the status
    int status = sql.get_status();
    std::string explanation = SQL::explain_status(status);
    // print response
    if (command.api)
    {
        // print using JSON
        std::string json = "{\"success\": ";
        json += status == SQL::STATUS_SUCCESS ? "true" : "false";
        json += ", \"explanation\": \"";
        json += SQL::explain_status(status);
        json += "\", \"length\": 0, \"data\": []}";
        std::cout << json << std::endl;
    }
    else
    {
        // print normally
        if (status == SQL::STATUS_SUCCESS)
        {
            std::cout << colors::green << "Ok, the table was deleted." << colors::reset << std::endl;
        }
        else
        {
            generic_error(command, SQL::explain_status(status));
        }
    }
}

void Driver::rename_table(command_data_t command)
{
    // pull tables from args
    std::string oldtable = command.args.at(0);
    std::string newtable = command.args.at(1);
    // make SQL engine
    SQL::SQLEngine sql = SQL::SQLEngine(command.database);
    // rename the table
    sql.rename_task_table(oldtable, newtable);
    // get the status
    int status = sql.get_status();
    std::string explanation = SQL::explain_status(status);
    // print response
    if (command.api)
    {
        // print using JSON
        std::string json = "{\"success\": ";
        json += status == SQL::STATUS_SUCCESS ? "true" : "false";
        json += ", \"explanation\": \"";
        json += SQL::explain_status(status);
        json += "\", \"length\": 0, \"data\": []}";
        std::cout << json << std::endl;
    }
    else
    {
        // print normally
        if (status == SQL::STATUS_SUCCESS)
        {
            std::cout << colors::green << "Ok, the table was renamed." << colors::reset << std::endl;
        }
        else
        {
            generic_error(command, SQL::explain_status(status));
        }
    }
}

void Driver::add_table(command_data_t command)
{
    // pull table from args
    std::string table = command.args.at(0);
    // make SQL engine
    SQL::SQLEngine sql = SQL::SQLEngine(command.database);
    // add task to the table
    sql.add_task_table(table);
    // get the status
    int status = sql.get_status();
    std::string explanation = SQL::explain_status(status);
    // print response
    if (command.api)
    {
        // print using JSON
        std::string json = "{\"success\": ";
        json += status == SQL::STATUS_SUCCESS ? "true" : "false";
        json += ", \"explanation\": \"";
        json += SQL::explain_status(status);
        json += "\", \"length\": 0, \"data\": []}";
        std::cout << json << std::endl;
    }
    else
    {
        // print normally
        if (status == SQL::STATUS_SUCCESS)
        {
            std::cout << colors::green << "Ok, the table was added." << colors::reset << std::endl;
        }
        else
        {
            generic_error(command, SQL::explain_status(status));
        }
    }
}

void Driver::list_tasks(command_data_t command)
{
    // pull table from args
    std::string table = command.args.at(0);
    // make SQL engine
    SQL::SQLEngine sql = SQL::SQLEngine(command.database);
    // get atlas entry
    atlas_t atlas = sql.get_task_table_info(table);
    if (sql.get_status() != SQL::STATUS_SUCCESS)
    {
        generic_error(command, SQL::explain_status(sql.get_status()));
        return;
    }
    // get tasks
    std::vector<task_t> tasks = sql.get_tasks_from_task_table(table);
    // check the API mode
    if (command.api)
    {
        // print in JSON format
        std::string json = "{\"success\": ";
        json += sql.get_status() == SQL::STATUS_SUCCESS ? "true" : "false";
        json += ", \"explanation\": \"";
        json += SQL::explain_status(sql.get_status());
        json += "\", \"length\": ";
        json += std::to_string(tasks.size() + 1);
        json += ", \"data\": [";
        json += "{";
        json += "\"name\": \"" + atlas.task_label + "\", ";
        json += "\"location\": \"" + atlas.location_label + "\", ";
        json += "\"date\": \"" + atlas.date_label + "\", ";
        json += "\"done\": \"" + atlas.done_label + "\", ";
        json += "\"hash\": \"" + (std::string)HASH_LABEL + "\"";
        json += tasks.size() == 0 ? "}" : "}, ";
        for (task_t i : tasks)
        {
            json += "{";
            json += "\"name\": \"" + i.name + "\", ";
            json += "\"location\": \"" + i.location + "\", ";
            json += "\"date\": \"" + dateLongToString(i.date) + "\", ";
            json += "\"done\": " + (std::string)(i.done ? "true" : "false") + ", ";
            json += "\"hash\": \"" + i.hash + "\"";
            json += "}, ";
        }
        if (tasks.size() > 0)
        {
            // if there were tasks, remove the ", " from the end of the list
            json.pop_back();
            json.pop_back();
        }
        json += "]}";
        std::cout << json << std::endl;
    }
    else
    {
        // print normally
        if (tasks.size() == 0)
        {
            // there are no items in the vector
            std::cout << colors::red << "There are no tasks in this table." << colors::reset << std::endl;
        }
        else
        {
            // print out all tasks in the vector
            long unsigned int maxDone = atlas.done_label.size();
            long unsigned int maxDate = atlas.date_label.size();
            long unsigned int maxLocation = atlas.location_label.size();
            long unsigned int maxName = atlas.task_label.size();
            long unsigned int maxHash = 0;
            for (task_t i : tasks)
            {
                if (i.name.size() > maxName)
                {
                    maxName = i.name.size();
                }
                if (i.location.size() > maxLocation)
                {
                    maxLocation = i.location.size();
                }
                if (dateLongToString(i.date).size() > maxDate)
                {
                    maxDate = dateLongToString(i.date).size();
                }
                if (i.hash.size() > maxHash)
                {
                    maxHash = i.hash.size();
                }
            }
            maxHash += TASKS_PADDING;
            maxDone += TASKS_PADDING;
            maxDate += TASKS_PADDING;
            maxLocation += TASKS_PADDING;
            std::cout << leftJustify(HASH_LABEL, maxHash);
            std::cout << leftJustify(atlas.done_label, maxDone);
            std::cout << leftJustify(atlas.date_label, maxDate);
            std::cout << leftJustify(atlas.location_label, maxLocation);
            std::cout << leftJustify(atlas.task_label, maxName);
            std::cout << std::endl;
            bool even = false;
            for (task_t i : tasks)
            {
                std::string out;
                out += leftJustify(i.hash, maxHash);
                out += leftJustify((i.done ? "Yes" : "No"), maxDone);
                out += leftJustify(dateLongToString(i.date), maxDate);
                out += leftJustify(i.location, maxLocation);
                out += leftJustify(i.name, maxName);
                if (i.done)
                {
                    std::cout << colors::dark_grey << out << colors::reset << std::endl;
                }
                else
                {
                    std::cout << (even ? colors::yellow : colors::light_blue) << out << colors::reset << std::endl;
                }
                even = !even;
            }
        }
    }
}

void Driver::edit_task(command_data_t command)
{
    // pull args out from command
    std::string table = command.args.at(0);
    std::string task = command.args.at(1);
    std::string column = command.args.at(2);
    std::string value = command.args.at(3);
    // get the column selector
    int selector = 0;
    if (stringEquals(column, "NAME"))
    {
        selector = SQL::COLUMN_NAME;
    }
    else if (stringEquals(column, "LOCATION"))
    {
        selector = SQL::COLUMN_LOCATION;
    }
    else if (stringEquals(column, "DATE"))
    {
        selector = SQL::COLUMN_DATE;
    }
    else if (stringEquals(column, "DONE"))
    {
        selector = SQL::COLUMN_DONE;
    }
    else
    {
        generic_error(command, "Invalid column selector.");
        return;
    }
    // make SQL engine
    SQL::SQLEngine sql = SQL::SQLEngine(command.database);
    // edit the task
    sql.edit_task(table, task, selector, value);
    // check status
    int status = sql.get_status();
    std::string explanation = SQL::explain_status(status);
    // ensure the date is correct
    if (column == "DATE")
    {
        // get the task from the ID
        for (task_t t : sql.get_tasks_from_task_table(table))
        {
            if (t.hash == task)
            {
                task = fix_dates(&sql, table, t.name, t.location, value, t.done, &status, &explanation);
            }
        }
    }
    // print response
    if (command.api)
    {
        // print using JSON
        std::string json = "{\"success\": ";
        json += status == SQL::STATUS_SUCCESS ? "true" : "false";
        json += ", \"explanation\": \"";
        json += explanation;
        json += "\", \"length\": 1, \"data\": [\"" + task + "\"]}";
        std::cout << json << std::endl;
    }
    else
    {
        // print normally
        if (status == SQL::STATUS_SUCCESS)
        {
            std::cout << colors::green << "Ok, the task was updated successfully." << colors::reset << std::endl;
        }
        else
        {
            generic_error(command, explanation);
        }
    }
}

void Driver::add_task(command_data_t command)
{
    // pull out args from command
    std::string table = command.args.at(0);
    std::string name = command.args.at(1);
    std::string location = command.args.at(2);
    std::string dateString = command.args.at(3);
    std::string doneString = command.args.at(4);
    // validate the date
    if (!isValidDateString(dateString))
    {
        generic_error(command, "Invalid date format.");
        return;
    }
    // validate the completion status
    if (!isValidDoneString(doneString))
    {
        generic_error(command, "Invalid completion status.");
        return;
    }
    // convert the date
    long dateLong = dateStringToLong(dateString);
    // convert the done string
    bool doneBool = atoi(doneStringToSQL(doneString).c_str());
    // make SQL engine
    SQL::SQLEngine sql = SQL::SQLEngine(command.database);
    // add the task
    sql.add_task(table, name, location, dateLong, doneBool);
    // get the status
    int status = sql.get_status();
    std::string explanation = SQL::explain_status(status);
    // convert date if it does not match
    fix_dates(&sql, table, name, location, dateString, doneBool, &status, &explanation);
    // print response
    if (command.api)
    {
        // print using JSON
        std::string json = "{\"success\": ";
        json += status == SQL::STATUS_SUCCESS ? "true" : "false";
        json += ", \"explanation\": \"";
        json += SQL::explain_status(status);
        json += "\", \"length\": 0, \"data\": []}";
        std::cout << json << std::endl;
    }
    else
    {
        // print normally
        if (status == SQL::STATUS_SUCCESS)
        {
            std::cout << colors::green << "Ok, the task was added." << colors::reset << std::endl;
        }
        else
        {
            generic_error(command, SQL::explain_status(status));
        }
    }
}

void Driver::delete_task(command_data_t command)
{
    // pull out args from command
    std::string table = command.args.at(0);
    std::string task_id = command.args.at(1);
    // make SQL engine
    SQL::SQLEngine sql = SQL::SQLEngine(command.database);
    // delete the task
    sql.delete_task(table, task_id);
    // get the status
    int status = sql.get_status();
    std::string explanation = SQL::explain_status(status);
    // print response
    if (command.api)
    {
        // print using JSON
        std::string json = "{\"success\": ";
        json += status == SQL::STATUS_SUCCESS ? "true" : "false";
        json += ", \"explanation\": \"";
        json += SQL::explain_status(status);
        json += "\", \"length\": 0, \"data\": []}";
        std::cout << json << std::endl;
    }
    else
    {
        // print normally
        if (status == SQL::STATUS_SUCCESS)
        {
            std::cout << colors::green << "Ok, the task was deleted." << colors::reset << std::endl;
        }
        else
        {
            generic_error(command, SQL::explain_status(status));
        }
    }
}

void Driver::show_help(command_data_t command)
{
    // generate help text
    std::vector<std::string> help;
    help.push_back(PROGRAM_USAGE);
    help.push_back(PROGRAM_DESCRIPTION);
    help.push_back("");
    help.push_back("Prefixes (optional)");
    help.push_back("  --api                                             print output as a JSON object");
    help.push_back("  --database FILE                                   use a custom database path");
    help.push_back("");
    help.push_back("Table Commands:");
    help.push_back("  --tables                                          list all tables");
    help.push_back("  --edit-label TABLE SELECTOR LABEL                 edit a label for a table");
    help.push_back("  --delete-table TABLE                              delete a table");
    help.push_back("  --rename-table TABLE NAME                         rename a table");
    help.push_back("  --add-table NAME                                  add a table");
    help.push_back("Task Commands:");
    help.push_back("  --tasks TABLE                                     list all tasks in a table");
    help.push_back("  --edit-task TABLE TASK SELECTOR VALUE             edit a task attribute");
    help.push_back("  --add-task TABLE NAME LOCATION DATE DONE          add a task");
    help.push_back("  --delete-task TABLE TASK                          delete a task");
    help.push_back("Information Commands:");
    help.push_back("  todo-cli --help                                   show help information");
    help.push_back("  todo-cli --usage                                  show command syntax");
    help.push_back("  todo-cli --version                                show version number");
    help.push_back("  todo-cli --license                                show license information");
    help.push_back("");
    help.push_back("Terminology:");
    help.push_back("  DATE                                              the date a task occurs (in the format YYYY-MM-DD HH:MM:SS)");
    help.push_back("  DONE                                              the completion status of a task (either true or false)");
    help.push_back("  FILE                                              a path to a file");
    help.push_back("  LABEL                                             a name used for labeling task attributes");
    help.push_back("  LOCATION                                          the location a task occurs");
    help.push_back("  NAME                                              the name of a table or task");
    help.push_back("  SELECTOR                                          a column position (see Selectors for possible values");
    help.push_back("  TABLE                                             the name of a table");
    help.push_back("  TASK                                              the ID for a specific task");
    help.push_back("  VALUE                                             either a NAME, LOCATION, DATE, or DONE depending on context");
    help.push_back("");
    help.push_back("Selectors:");
    help.push_back("  NAME                                              select the task or table name column");
    help.push_back("  LOCATION                                          select the location column");
    help.push_back("  DATE                                              select the date column");
    help.push_back("  DONE                                              select the completion status column");
    // determine mode for output
    if (command.api)
    {
        // print as JSON
        std::string json = "{\"success\": true, ";
        json += "\"explanation\":\"The command exited successfully.\", ";
        json += "\"length\": ";
        json += std::to_string(help.size());
        json += ", \"data\": [";
        for (std::string i : help)
        {
            json += "\"" + i + "\",";
        }
        if (help.size() > 0)
        {
            // if there were help strings, remove the "," from the end of the list
            json.pop_back();
        }
        json += "]}";
        std::cout << json << std::endl;
    }
    else
    {
        // print normally
        for (std::string str : help)
        {
            std::cout << str << std::endl;
        }
    }
}

void Driver::show_license(command_data_t command)
{
    // generate license text
    std::vector<std::string> license;
    license.push_back("To-Do CLI");
    license.push_back("Copyright (C) 2022 Cameron Himes");
    license.push_back("");
    license.push_back("This program is free software: you can redistribute it and/or modify");
    license.push_back("it under the terms of the GNU General Public License as published by");
    license.push_back("the Free Software Foundation, either version 3 of the License, or");
    license.push_back("(at your option) any later version.");
    license.push_back("");
    license.push_back("This program is distributed in the hope that it will be useful,");
    license.push_back("but WITHOUT ANY WARRANTY; without even the implied warranty of");
    license.push_back("MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the");
    license.push_back("GNU General Public License for more details.");
    license.push_back("");
    license.push_back("You should have received a copy of the GNU General Public License");
    license.push_back("along with this program. If not, see <http://www.gnu.org/licenses/>.");
    // determine mode for output
    if (command.api)
    {
        // print as JSON
        std::string json = "{\"success\": true, ";
        json += "\"explanation\":\"The command exited successfully.\", ";
        json += "\"length\": ";
        json += std::to_string(license.size());
        json += ", \"data\": [";
        for (std::string i : license)
        {
            json += "\"" + i + "\",";
        }
        if (license.size() > 0)
        {
            // if there were license strings, remove the "," from the end of the list
            json.pop_back();
        }
        json += "]}";
        std::cout << json << std::endl;
    }
    else
    {
        // print normally
        for (std::string str : license)
        {
            std::cout << str << std::endl;
        }
    }
}

void Driver::show_usage(command_data_t command)
{
    // determine mode for output
    if (command.api)
    {
        // print as JSON
        std::string json = "{\"success\": true, ";
        json += "\"explanation\":\"The command exited successfully.\", ";
        json += "\"length\": 1, ";
        json += "\"data\": [\"" + (std::string)PROGRAM_USAGE + "\"]}";
        std::cout << json << std::endl;
    }
    else
    {
        // print normally
        std::cout << PROGRAM_USAGE << std::endl;
    }
}

void Driver::show_version(command_data_t command)
{
    // determine mode for output
    if (command.api)
    {
        // print as JSON
        std::string json = "{\"success\": true, ";
        json += "\"explanation\":\"The command exited successfully.\", ";
        json += "\"length\": 1, ";
        json += "\"data\": [\"" + (std::string)PROGRAM_VERSION + "\"]}";
        std::cout << json << std::endl;
    }
    else
    {
        // print normally
        std::cout << PROGRAM_VERSION << std::endl;
    }
}

void Driver::internal_test(command_data_t command)
{
    std::cout << "API: " << command.api << std::endl;
    std::cout << "Command: " << ARGS::commandToString(command.command) << std::endl;
    std::cout << "Database: " << command.database << std::endl;
    std::cout << "Args: [";
    for (auto i : command.args)
    {
        std::cout << i << ", ";
    }
    std::cout << "\b\b]" << std::endl;
    // test SQL stuff
    SQL::SQLEngine s = SQL::SQLEngine(command.database);
    s.add_task_table("school");
    s.add_task_table("work");
    s.add_task_table("personal");
    std::string id_sch = s.get_task_table_id_from_name("school");
    std::string id_per = s.get_task_table_id_from_name("personal");
    std::cout << "ID of school is " << id_sch << std::endl;
    std::cout << "ID of personal is " << id_per << std::endl;
    s.delete_task_table("personal");
    s.edit_task_table(SQL::COLUMN_NAME, "assignment", "school");
    s.edit_task_table(SQL::COLUMN_LOCATION, "class", "school");
    s.edit_task_table(SQL::COLUMN_DATE, "due", "school");
    s.edit_task_table(SQL::COLUMN_DONE, "submitted", "school");
    std::vector<task_t> tasks = s.get_tasks_from_task_table("school");
    std::cout << "Return vector has " << tasks.size() << " items:" << std::endl;
    for (task_t t : tasks)
    {
        std::cout << "\t" << t.name << ", " << t.location << ", " << dateLongToString(t.date) << " (" << t.date << "), " << t.done << ", " << t.hash << std::endl;
    }
    s.add_task("school", "test", "asulearn", 2064350876, false);
    s.delete_task("school", "1639517517831577645");
    s.edit_task("school", "BGDJFCBAGIHBDGHEDAB", SQL::COLUMN_NAME, "test_name");
    s.edit_task("school", "BGDJFCBAGIHBDGHEDAB", SQL::COLUMN_LOCATION, "test_location");
    s.edit_task("school", "BGDJFCBAGIHBDGHEDAB", SQL::COLUMN_DATE, "2939195277");
    s.edit_task("school", "BGDJFCBAGIHBDGHEDAB", SQL::COLUMN_DONE, "1");
    std::cout << "Return vector has " << tasks.size() << " items:" << std::endl;
    for (task_t t : tasks)
    {
        std::cout << "\t" << t.name << ", " << t.location << ", " << dateLongToString(t.date) << " (" << t.date << "), " << t.done << ", " << t.hash << std::endl;
    }
    atlas_t school_info = s.get_task_table_info("school");
    std::cout << "School table information:" << std::endl;
    std::cout << "name: " << school_info.name << std::endl;
    std::cout << "task_label: " << school_info.task_label << std::endl;
    std::cout << "location_label: " << school_info.location_label << std::endl;
    std::cout << "date_label: " << school_info.date_label << std::endl;
    std::cout << "done_label: " << school_info.done_label << std::endl;
    std::cout << "table_id: " << school_info.table_id << std::endl;
    // check time conversion stuff
    std::cout << "2021-12-10 23:01:17 is " << dateStringToLong("2021-12-10 23:01:17") << " (answer: 1639195277 for EST)" << std::endl;
    std::cout << "2021-12-10 23:01:17 == " << dateLongToString(dateStringToLong("2021-12-10 23:01:17")) << std::endl;
    std::cout << "1639195277 == " << dateStringToLong(dateLongToString(1639195277)) << std::endl;
    // string validation stuff
    std::string buffer;
    while (true)
    {
        std::cout << "Enter string: ";
        std::getline(std::cin, buffer);
        std::cout << "isValidTableName: " << (isValidTableName(buffer) ? "YES" : "NO") << std::endl;
        std::cout << "isValidTaskName: " << (isValidTaskName(buffer) ? "YES" : "NO") << std::endl;
        std::cout << "isValidLocationName: " << (isValidLocationName(buffer) ? "YES" : "NO") << std::endl;
        std::cout << "isValidDateString: " << (isValidDateString(buffer) ? "YES" : "NO") << std::endl;
        std::cout << "isValidDoneString: " << (isValidDoneString(buffer) ? "YES" : "NO") << std::endl;
        std::cout << "isNumber: " << (isNumber(buffer) ? "YES" : "NO") << std::endl;
        if (stringEquals(buffer, ".exit"))
        {
            break;
        }
    }
}

void Driver::error_no_args(command_data_t command)
{
    generic_error(command, "No command was provided.");
}

void Driver::error_few_args(command_data_t command)
{
    generic_error(command, "There are not enough arguments for the provided command.");
}

void Driver::error_excess_args(command_data_t command)
{
    generic_error(command, "There are too many arguments for the provided command.");
}

void Driver::error_invalid(command_data_t command)
{
    generic_error(command, "The provided command does not exist.");
}

void Driver::error_database_file(command_data_t command)
{
    generic_error(command, "The database file could not be opened.");
}

void Driver::run(command_data_t command)
{
    // check the file path now, so it doesn't crash inside SQLEngine
    // check if file path is valid
    if (!isValidFilePath(command.database))
    {
        error_database_file(command);
        return;
    }
    // determine the command and run it
    switch (command.command)
    {
    case ARGS::COMMAND_TABLES:
        list_tables(command);
        break;
    case ARGS::COMMAND_EDIT_LABEL:
        edit_table(command);
        break;
    case ARGS::COMMAND_DELETE_TABLE:
        delete_table(command);
        break;
    case ARGS::COMMAND_RENAME_TABLE:
        rename_table(command);
        break;
    case ARGS::COMMAND_ADD_TABLE:
        add_table(command);
        break;
    case ARGS::COMMAND_TASKS:
        list_tasks(command);
        break;
    case ARGS::COMMAND_EDIT_TASK:
        edit_task(command);
        break;
    case ARGS::COMMAND_ADD_TASK:
        add_task(command);
        break;
    case ARGS::COMMAND_DELETE_TASK:
        delete_task(command);
        break;
    case ARGS::COMMAND_HELP:
        show_help(command);
        break;
    case ARGS::COMMAND_USAGE:
        show_usage(command);
        break;
    case ARGS::COMMAND_VERSION:
        show_version(command);
        break;
    case ARGS::COMMAND_LICENSE:
        show_license(command);
        break;
    case ARGS::COMMAND_TEST:
        internal_test(command);
        break;
    case ARGS::COMMAND_ERROR_NO_ARGS:
        error_no_args(command);
        break;
    case ARGS::COMMAND_ERROR_FEW_ARGS:
        error_few_args(command);
        break;
    case ARGS::COMMAND_ERROR_EXCESS_ARGS:
        error_excess_args(command);
        break;
    case ARGS::COMMAND_ERROR_INVALID:
        error_invalid(command);
        break;
    }
}

void Driver::generic_error(command_data_t command, std::string error)
{
    // check if it should be in API format
    if (command.api)
    {
        // print error as JSON
        std::string response = "{\"success\": false, \"explanation\": \"" + error + "\", \"length\": 0, \"data\": []}";
        std::cout << response << std::endl;
    }
    else
    {
        // print error normally
        crash(colors::red + error + colors::reset);
    }
}

std::string Driver::fix_dates(SQL::SQLEngine *sql, std::string table, std::string name, std::string location, std::string date, bool done, int *status, std::string *explanation)
{
    // make container for new hash
    std::string hash;
    // make the dateLong
    long dateLong = dateStringToLong(date);
    // convert date if it does not match
    std::vector<task_t> tasks = sql->get_tasks_from_task_table(table);
    for (task_t old_task : tasks)
    {
        // find the task in question
        if (old_task.name == name && old_task.location == location && old_task.date == dateLong && old_task.done == done)
        {
            // found the task, now get the offset
            // NOTE: the time issue is only exposed when converted to a string
            long offset = dateStringToLong(dateLongToString(old_task.date)) - dateLong;
            // store hash for API stuff
            hash = old_task.hash;
            // check if a new task is needed
            if (offset)
            {
                // calculate the new time
                dateLong = old_task.date - offset;
                // delete the old task and add a new one
                sql->delete_task(table, old_task.hash);
                sql->add_task(table, name, location, dateLong, done);
                *(status) = sql->get_status();
                *(explanation) = SQL::explain_status(*(status));
                // get hash of new task
                for (task_t new_task : sql->get_tasks_from_task_table(table)) {
                    if (new_task.name == name && new_task.location == location && new_task.date == dateLong && new_task.done == done) {
                        hash = new_task.hash;
                    }
                }
            }
            // exit loop
            break;
        }
    }
    // return the new hash
    return hash;
}
