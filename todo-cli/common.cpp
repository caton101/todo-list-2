/*
 * To-Do CLI
 * Copyright (C) 2022 Cameron Himes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <string>
#include <iostream>
#include <vector>
#include <chrono>
#include <fstream>
#include <iomanip>
#include "common.hpp"

/**
 * @brief  This shows an error message and crashes.
 * @param  message: the error message
 * @retval None
 */
void crash(std::string message)
{
    std::cerr << message << std::endl;
    exit(1);
}

/**
 * @brief  This checks if two strings are equal.
 * @param  a: the first string
 * @param  b: the second string
 * @retval true if the strings are the same
 */
bool stringEquals(std::string a, std::string b)
{
    return !a.compare(b);
}

/**
 * @brief  This searches a vector for a string.
 * @param  vector: the vector of string
 * @param  string: the string to find
 * @retval true if string is inside vector
 */
bool isStringInsideVector(std::vector<std::string> vector, std::string string)
{
    // try to find the string inside the vector
    for (std::string i : vector)
    {
        if (i.compare(string) == 0)
        {
            // string was found
            return true;
        }
    }
    // string was not found
    return false;
}

/**
 * @brief  This searches a vector for a task.
 * @note   This does not compare DONE or HASH fields
 * @param  vector: the vector of tasks
 * @param  task: the task to find
 * @retval true if string is inside vector
 */
bool isTaskInsideVector(std::vector<task_t> vector, task_t task)
{
    // try to find the task inside the vector
    for (task_t i : vector)
    {
        if (stringEquals(i.name, task.name) && stringEquals(i.location, task.location) && (i.date == task.date))
        {
            // task was found
            return true;
        }
    }
    // task was not found
    return false;
}

/**
 * @brief  This checks if a task hash exists in a vector of tasks
 * @param  vector: the vector of tasks
 * @param  hash: the task hash to locate
 * @retval true if the there is a task in the vector with the specified hash
 */
bool isTaskHashInVector(std::vector<task_t> vector, std::string hash)
{
    // try to match the task id
    for (task_t i : vector)
    {
        if (stringEquals(i.hash, hash))
        {
            // id was found
            return true;
        }
    }
    // id was not found
    return false;
}

/**
 * @brief  This converts a timestamp string (local) to a long (GMT)
 * @param  dateStr: the timestamp string in local timezone
 * @retval the timestamp as a long in GMT timezone
 */
long dateStringToLong(std::string dateStr)
{
    // This code would NOT be possible without this amazing person: https://stackoverflow.com/a/60231637
    // all credit for this code goes to them
    std::tm timeDate = {};
    std::istringstream ss(dateStr);
    ss >> std::get_time(&timeDate, "%Y-%m-%d %H:%M:%S");
    std::chrono::time_point<std::chrono::system_clock> timePoint = std::chrono::system_clock::from_time_t(mktime(&timeDate));
    return std::chrono::duration_cast<std::chrono::seconds>(timePoint.time_since_epoch()).count();
}

/**
 * @brief  This converts a timestamp long (GMT) to a string (localtime)
 * @param  dateInt: the timestamp long in GMT timezone
 * @retval the timestamp string in local timezone
 */
std::string dateLongToString(long dateInt)
{
    // This code would NOT be possible without this amazing person: https://stackoverflow.com/a/60231637
    // all credit for this code goes to them
    std::time_t now_c = dateInt;
    auto tm = std::localtime(&now_c);
    char buffer[32];
    std::strftime(buffer, 32, "%Y-%m-%d %H:%M:%S", tm);
    return std::string(buffer);
}

/**
 * @brief  This checks if a table name is legal
 * @note   allows a single letter followed by any number of letters or numbers
 * @param  table: the table name to check
 * @retval true if string is a legal table name
 */
bool isValidTableName(std::string table)
{
    // table name MUST not be empty
    if (!table.size())
    {
        // table string is empty
        return false;
    }
    // all characters MUST be letters or numbers
    for (std::size_t i = 0; i < table.length(); i++)
    {
        char c = table[i];
        // do NOT allow numbers for first iteraton
        if (i == 0 && (c >= '0' && c <= '9'))
        {
            return false;
        }
        // allow [A-Z]+[a-z]+[0-9]
        if (!((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || (c >= '0' && c <= '9')))
        {
            return false;
        };
    }
    // all characters must be valid here
    return true;
}

/**
 * @brief  This checks if a column label is legal
 * @note   allows any number of letters, numbers, or certain special characters
 * @param  label: the label to check
 * @retval true if the string is a legal column label
 */
bool isValidColumnLabel(std::string label)
{
    // validate according to task name rules
    return isValidTaskName(label);
}

/**
 * @brief  This checks if a task name is legal
 * @note   allows any number of letters, numbers, or certain special characters
 * @param  table: the task name to check
 * @retval true if string is a legal task name
 */
bool isValidTaskName(std::string name)
{
    // do not allow empty string
    if (name.empty()) {
        return false;
    }
    // all characters can be spaces, parentheses, punctuation, numbers, or letters
    std::string specials = " ~!@#$%^&*()-_=+[]{}|;:<>,./?";
    for (char c : name)
    {
        if (!((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || (c >= '0' && c <= '9') || (specials.find(c) != specials.npos)))
        {
            return false;
        };
    }
    // do not allow string to start or end with a special character
    if (specials.find(name.at(0)) != std::string::npos || specials.find(name.at(name.length()-1)) != std::string::npos) {
        return false;
    }
    // all characters are valid here
    return true;
}

/**
 * @brief  This checks if a task location is legal
 * @note   allows any number of letters, numbers, or certain special characters
 * @param  table: the task location to check
 * @retval true if string is a legal task location
 */
bool isValidLocationName(std::string location)
{
    // validate according to task name rules
    return isValidTaskName(location);
}

/**
 * @brief  This checks if a task date is legal
 * @note   a date MUST be YYYY-MM-DD HH:MM:SS or YYYY-MM-DD HH:MM or YYYY-MM-DD HH or YYYY-MM-DD
 * @param  date: the date string
 * @retval true if string is both correctly formatted and contains valid numbers
 */
bool isValidDateString(std::string date)
{
    // a date MUST be YYYY-MM-DD HH:MM:SS or YYYY-MM-DD HH:MM or YYYY-MM-DD HH or YYYY-MM-DD
    if (date.length() < 10)
    {
        // date is too short
        return false;
    }
    // check if something is wrong with the date
    for (int i = 0; i < 10; i++)
    {
        // determine if number or dash
        if (i == 4 || i == 7)
        {
            if (date.at(i) != '-')
            {
                return false;
            }
            else
            {
                continue;
            }
        }
        if (!isNumber(date.at(i)))
        {
            return false;
        }
    }
    // validate the date
    int year = atoi(date.substr(0, 4).c_str());
    int month = atoi(date.substr(5, 2).c_str());
    int days = atoi(date.substr(8, 2).c_str());
    if (month < 1 || month > 12)
    {
        // month is invalid
        return false;
    }
    if (days < 1 || days > getDaysInMonth(year, month))
    {
        // day is invalid
        return false;
    }
    // check if this string is long enough to contain hours
    if (date.length() == 10)
    {
        // the string is only a date
        return true;
    }
    if (date.length() < 13)
    {
        // date is too short for an hour
        return false;
    }
    if (date.at(10) != ' ' || !isNumber(date.at(11)) || !isNumber(date.at(12)))
    {
        // the hour is wrong
        return false;
    }
    // validate the hour
    int hour = atoi(date.substr(11, 2).c_str());
    if (hour > 23)
    {
        // hour is invalid
        return false;
    }
    // check if this string is long enough to contain minutes
    if (date.length() == 13)
    {
        // the string is only a date and hour
        return true;
    }
    if (date.length() < 16)
    {
        // the string is too short for a date, hour, and minute
        return false;
    }
    if (date.at(13) != ':' || !isNumber(date.at(14)) || !isNumber(date.at(15)))
    {
        // the minute is wrong
        return false;
    }
    // validate the minute
    int minute = atoi(date.substr(14, 2).c_str());
    if (minute > 59)
    {
        // minute is wrong
        return false;
    }
    // check if this string is long enough to contain seconds
    if (date.length() == 16)
    {
        // the string is only a date, hour, and minute
    }
    if (date.length() < 19)
    {
        // the string is too short for a date, hour, minute, and second
        return false;
    }
    if (date.at(16) != ':' || !isNumber(date.at(17)) || !isNumber(date.at(18)))
    {
        // the second is wrong
        return false;
    }
    // validate the second
    int second = atoi(date.substr(17, 2).c_str());
    if (second > 59)
    {
        // second is wrong
        return false;
    }
    // check if there are tailing values
    if (date.length() > 19)
    {
        // string is too big
        return false;
    }
    // date is correct
    return true;
}

/**
 * @brief  This checks if a date long is legal
 * @param  date: the date long to check
 * @retval true if the date long is valid
 */
bool isValidDateLong(long date)
{
    // NOTE: this number was found by trial and error
    return date <= 253402300799;
}

/**
 * @brief  This checks if a task done status is legal
 * @param  done: the done status to verify
 * @retval true if done status is valid
 */
bool isValidDoneString(std::string done)
{
    // allow SOME specific strings
    std::string up;
    for (char c : done)
    {
        up += toupper(c);
    }
    return stringEquals(up, "TRUE") || stringEquals(up, "FALSE") || stringEquals(up, "1") || stringEquals(up, "0");
}

/**
 * @brief  This checks if a file path is valid
 * @param  path: the file path to open
 * @retval true if the file path is valid
 */
bool isValidFilePath(std::string path)
{
    // attempt to open the file for appending
    std::fstream stream;
    stream.open(path.c_str(), std::ios::app);
    if (!stream)
    {
        // file did not open
        return false;
    }
    // file was opened
    return true;
}

/**
 * @brief  This ensures the completion status is valid SQL
 * @param  done: the current completion status
 * @retval a SQL safe completion status
 */
std::string doneStringToSQL(std::string done)
{
    // convert to upper case
    std::string up;
    for (char c : done)
    {
        up += toupper(c);
    }
    // override true and false
    if (stringEquals(up, "TRUE"))
    {
        return "1";
    }
    else if (stringEquals(up, "FALSE"))
    {
        return "0";
    }
    else
    {
        return done;
    }
}

/**
 * @brief  This checks if a character is a number
 * @param  character: the character to check
 * @retval true if the character is a number
 */
bool isNumber(char character)
{
    return character >= '0' && character <= '9';
}

/**
 * @brief  This checks if a string is a number
 * @param  number: the string to check
 * @retval true if the string is a number
 */
bool isNumber(std::string number)
{
    bool status = true;
    for (char c : number)
    {
        status = status && isNumber(c);
    }
    return status;
}

/**
 * @brief  This checks if a year is a leap year
 * @param  year: the year to check
 * @retval true if the year is a leap year
 */
bool isLeapYear(int year)
{
    // reasoning is from: https://www.wikihow.com/Calculate-Leap-Years#Using-Division
    bool isDiv4 = !(year % 4);
    bool isDiv100 = !(year % 100);
    bool isDiv400 = !(year % 400);
    if (isDiv4 && !isDiv100)
    {
        // this is a leap year
        return true;
    }
    if (isDiv100 && isDiv400)
    {
        // this is also a leap year
        return true;
    }
    // it is not a leap year
    return false;
}

/**
 * @brief  This computes the number of days in a month
 * @note   the year only matters if you check February
 * @param  year: the year to check
 * @param  month: the month to check
 * @retval the number of days in the given month
 */
int getDaysInMonth(int year, int month)
{
    //                          J   F   M   A   M   J   J   A   S   O   N   D
    // int DAYS_IN_MONTH[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    //                          1   2   3   4   5   6   7   8   9   10  11  12
    int days;
    switch (month)
    {
    case 1:
    case 3:
    case 5:
    case 7:
    case 8:
    case 10:
    case 12:
        days = 31;
        break;
    case 4:
    case 6:
    case 9:
    case 11:
        days = 30;
        break;
    case 2:
        days = isLeapYear(year) ? 29 : 28;
        break;
    default:
        crash("Illegal month value passed to getDaysInMonth.");
        break;
    }
    return days;
}

/**
 * @brief  This expands ~ in a file path if needed
 * @note   relies on HOME to get a file path
 * @param  path: the relative file path
 * @retval the full file path
 */
std::string fixFilePath(std::string path)
{
    std::string newPath;
    for (char c : path)
    {
        if (c == '~')
        {
            newPath += getHome();
        }
        else
        {
            newPath += c;
        }
    }
    return newPath;
}

/**
 * @brief  This tries to get the user's home directory
 * @note   this crashes if HOME is not set
 * @retval the path of the user's home directory
 */
std::string getHome()
{
    char *homeenv = getenv("HOME");
    if (!homeenv)
    {
        crash("Could not get home directory.");
    }
    return homeenv;
}

/**
 * @brief  This adds whitespace on the right side of a string
 * @note   returns the original if provided text is larger
 * @param  text: the string to expand
 * @param  size: the new size of the string
 * @retval the original string with whitespace on the right
 */
std::string leftJustify(std::string text, long unsigned int size)
{
    // check if text is already bigger than the specified size
    if (text.size() >= size)
    {
        return text;
    }
    // expand the string
    std::string buffer = text;
    for (std::size_t i = 0; i < (size - text.size()); i++)
    {
        buffer += " ";
    }
    return buffer;
}
