/*
 * To-Do CLI
 * Copyright (C) 2022 Cameron Himes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * CREDITS
 *
 * This file was made using the following guides:
 * https://www.geeksforgeeks.org/sql-using-c-c-and-sqlite/
 * https://www.sqlite.org/datatype3.html
 * https://www.sqlite.org/lang_datefunc.html
 */

#include <sqlite3.h>
#include <iostream>
#include <chrono>
#include <vector>
#include "sql.hpp"
#include "common.hpp"
#include "config.hpp"

/**
 * @brief  This is the callback helper for get_task_table_names
 * @note   DO NOT CALL THIS DIRECTLY
 * @param  data: the memory address to store information
 * @param  cols: the number of columns to read
 * @param  colValues: the names of columns
 * @param  colNames: the values stored in columns
 * @retval the callback status
 */
int SQL::callback_get_task_table_names(void *data, int cols, char **colValues, char **colNames)
{
    // cast container back to a vector
    std::vector<std::string> *result = static_cast<std::vector<std::string> *>(data);
    // get the NAME column
    for (int i = 0; i < cols; i++)
    {
        if (stringEquals(colNames[i], "NAME"))
        {
            result->push_back(colValues[i]);
        }
    }
    // return OK
    return 0;
}

/**
 * @brief  This is the callback helper for get_task_table_id_from_name
 * @note   DO NOT CALL THIS DIRECTLY
 * @param  data: the memory address to store information
 * @param  cols: the number of columns to read
 * @param  colValues: the names of columns
 * @param  colNames: the values stored in columns
 * @retval the callback status
 */
int SQL::callback_get_task_table_id_from_name(void *data, int cols, char **colValues, char **colNames)
{
    // cast container back to string
    std::string *id = static_cast<std::string *>(data);
    // get the TABLE_ID column
    for (int i = 0; i < cols; i++)
    {
        if (stringEquals(colNames[i], "TABLE_ID"))
        {
            id->assign(colValues[i]);
        }
    }
    // return OK
    return 0;
}

/**
 * @brief  This is the callback helper for get_tasks_from_task_table
 * @note   DO NOT CALL THIS DIRECTLY
 * @param  data: the memory address to store information
 * @param  cols: the number of columns to read
 * @param  colValues: the names of columns
 * @param  colNames: the values stored in columns
 * @retval the callback status
 */
int SQL::callback_get_tasks_from_task_table(void *data, int cols, char **colValues, char **colNames)
{
    // cast container back to vector
    std::vector<task_t> *results = static_cast<std::vector<task_t> *>(data);
    // make container for result
    task_t result;
    // get the TABLE_ID column
    for (int i = 0; i < cols; i++)
    {
        if (stringEquals(colNames[i], "NAME"))
        {
            result.name = colValues[i];
        }
        else if (stringEquals(colNames[i], "LOCATION"))
        {
            result.location = colValues[i];
        }
        else if (stringEquals(colNames[i], "HASH"))
        {
            result.hash = colValues[i];
        }
        else if (stringEquals(colNames[i], "strftime(\"%s\",DATE)"))
        {
            result.date = atol(colValues[i]);
        }
        else if (stringEquals(colNames[i], "DONE"))
        {
            result.done = atoi(colValues[i]);
        }
        else
        {
            crash((std::string) "Got unexpected column \"" + colNames[i] + "\" when reading task list.");
        }
    }
    // add task to vector
    results->push_back(result);
    // return OK
    return 0;
}

/**
 * @brief  This is the callback helper for get_task_table_info
 * @note   DO NOT CALL THIS DIRECTLY
 * @param  data: the memory address to store information
 * @param  cols: the number of columns to read
 * @param  colValues: the names of columns
 * @param  colNames: the values stored in columns
 * @retval the callback status
 */
int SQL::callback_get_task_table_info(void *data, int cols, char **colValues, char **colNames)
{
    // cast data back to an atlas entry
    atlas_t *target = static_cast<atlas_t *>(data);
    // get the TABLE_ID column
    for (int i = 0; i < cols; i++)
    {
        if (stringEquals(colNames[i], "NAME"))
        {
            target->name.assign(colValues[i]);
        }
        else if (stringEquals(colNames[i], "TASK_LABEL"))
        {
            target->task_label.assign(colValues[i]);
        }
        else if (stringEquals(colNames[i], "LOCATION_LABEL"))
        {
            target->location_label.assign(colValues[i]);
        }
        else if (stringEquals(colNames[i], "DATE_LABEL"))
        {
            target->date_label.assign(colValues[i]);
        }
        else if (stringEquals(colNames[i], "DONE_LABEL"))
        {
            target->done_label.assign(colValues[i]);
        }
        else if (stringEquals(colNames[i], "TABLE_ID"))
        {
            target->table_id.assign(colValues[i]);
        }
        else
        {
            crash((std::string) "Got unexpected column \"" + colNames[i] + "\" when reading task list.");
        }
    }
    // return OK
    return 0;
}

/**
 * @brief  This takes a status code and returns an explanation
 * @param  status: the STATUS_* code
 * @retval a string explaining the status code
 */
std::string SQL::explain_status(int status)
{
    std::string explanation;
    switch (status)
    {
    case STATUS_SUCCESS:
        explanation = "The command exited successfully.";
        break;
    case STATUS_INVALID_TABLE_NAME:
        explanation = "An illegal table name was provided.";
        break;
    case STATUS_INVALID_TASK_NAME:
        explanation = "An illegal task name was provided.";
        break;
    case STATUS_INVALID_TASK_LOCATION:
        explanation = "An illegal task location was provided.";
        break;
    case STATUS_INVALID_TASK_DATE:
        explanation = "An illegal task date was provided.";
        break;
    case STATUS_INVALID_TASK_DONE:
        explanation = "An illegal done status was provided.";
        break;
    case STATUS_INVALID_COLUMN_SELECTOR:
        explanation = "An invalid column selector was provided.";
        break;
    case STATUS_INVALID_COLUMN_NAME:
        explanation = "An invalid column name was provided.";
        break;
    case STATUS_TABLE_NOT_EXIST:
        explanation = "The provided table does not exist.";
        break;
    case STATUS_TABLE_ALREADY_EXISTS:
        explanation = "The provided table already exists.";
        break;
    case STATUS_TASK_ALREADY_EXISTS:
        explanation = "The provided task already exists.";
        break;
    case STATUS_HASH_NOT_EXIST:
        explanation = "The provided id does not exist.";
        break;
    default:
        explanation = "Called explain_status with invalid status code.";
        break;
    }
    return explanation;
}

/**
 * @brief  This constructs a SQLEngine object
 * @param  filepath: the database file to create/open
 * @retval a new SQLEngine object
 */
SQL::SQLEngine::SQLEngine(std::string filepath)
{
    // set the file path
    this->filepath = filepath;
    // attempt to open database
    open_database();
    // ensure ATLAS exists
    generate_atlas();
}

/**
 * @brief  This deconstructs a SQLEngine object
 * @retval None
 */
SQL::SQLEngine::~SQLEngine()
{
    // close the database
    close_database();
}

/**
 * @brief  This gets the SQL status
 * @retval the SQL status
 */
int SQL::SQLEngine::get_status()
{
    return this->sql_status;
}

/**
 * @brief  This gets a list of task table names
 * @note   This also sets the status code
 * @retval a vector of task table names
 */
std::vector<std::string> SQL::SQLEngine::get_task_table_names()
{
    // make response vector
    std::vector<std::string> response;
    // run query
    std::string cmd = "SELECT name FROM ATLAS;";
    this->exit_status = sqlite3_exec(this->database, cmd.c_str(), callback_get_task_table_names, &response, &this->message);
    check_error_message("Failed to get rows from ATLAS");
    // return response vector
    this->sql_status = STATUS_SUCCESS;
    return response;
}

/**
 * @brief  This converts a task table name to a task table hash
 * @note   This also sets the status code
 * @param  name: the task table name
 * @retval the task table hash
 */
std::string SQL::SQLEngine::get_task_table_id_from_name(std::string name)
{
    // container for id
    std::string id;
    // check if the name exists
    if (!isStringInsideVector(get_task_table_names(), name))
    {
        // name does not exist
        this->sql_status = STATUS_TABLE_NOT_EXIST;
        id = "";
    }
    else
    {
        // name does exist
        std::string cmd = "SELECT TABLE_ID FROM ATLAS WHERE NAME=\"" + name + "\";";
        this->exit_status = sqlite3_exec(this->database, cmd.c_str(), callback_get_task_table_id_from_name, &id, &this->message);
        check_error_message("Failed to get table id from ATLAS");
        this->sql_status = STATUS_SUCCESS;
    }
    // return the id
    return id;
}

/**
 * @brief  This adds a task table to the database
 * @note   This also sets the status code
 * @param  name: the name of the task table
 * @retval None
 */
void SQL::SQLEngine::add_task_table(std::string name)
{
    // check if name is valid
    if (!isValidTableName(name))
    {
        // set error and return
        this->sql_status = STATUS_INVALID_TABLE_NAME;
        return;
    }
    // check if it already exists
    if (isStringInsideVector(get_task_table_names(), name))
    {
        // set error and return
        this->sql_status = STATUS_TABLE_ALREADY_EXISTS;
        return;
    }
    // make command string
    std::string cmd;
    // get an ID
    std::string id = generate_id();
    // add table to ATLAS
    cmd = "INSERT INTO ATLAS VALUES (\"" + name + "\",\"" + DEFAULT_NAME_LABEL + "\",\"" + DEFAULT_LOCATION_LABEL + "\",\"" + DEFAULT_DATE_LABEL + "\",\"" + DEFAULT_DONE_LABEL + "\",\"" + id + "\");";
    this->exit_status = sqlite3_exec(this->database, cmd.c_str(), NULL, 0, &this->message);
    check_error_message("Failed to insert row into ATLAS");
    // generate table
    cmd = "CREATE TABLE IF NOT EXISTS \"" + id + "\" ("
                                                 "NAME     STRING  NOT NULL,"
                                                 "LOCATION STRING  NOT NULL,"
                                                 "DATE     STRING  NOT NULL,"
                                                 "DONE     STRING  NOT NULL,"
                                                 "HASH     STRING  NOT NULL);";
    this->exit_status = sqlite3_exec(this->database, cmd.c_str(), NULL, 0, &this->message);
    check_error_message("Failed to create new task table");
    this->sql_status = STATUS_SUCCESS;
}

/**
 * @brief  This removes a task table from the database
 * @note   This also sets the status code
 * @param  name: the name of the task table
 * @retval None
 */
void SQL::SQLEngine::delete_task_table(std::string name)
{
    // check if the table name is legal
    if (!isValidTableName(name))
    {
        // set error and return
        this->sql_status = STATUS_INVALID_TABLE_NAME;
        return;
    }
    // check if table exists
    if (!isStringInsideVector(get_task_table_names(), name))
    {
        // set error and return
        this->sql_status = STATUS_TABLE_NOT_EXIST;
        return;
    }
    // make command string
    std::string cmd;
    // get the id
    std::string id = get_task_table_id_from_name(name);
    // delete the table
    cmd = "DROP TABLE IF EXISTS \"" + id + "\";";
    this->exit_status = sqlite3_exec(this->database, cmd.c_str(), NULL, 0, &this->message);
    check_error_message("Failed to delete the task list");
    // remove row from ATLAS
    cmd = "DELETE FROM ATLAS WHERE NAME='" + name + "';";
    this->exit_status = sqlite3_exec(this->database, cmd.c_str(), NULL, 0, &this->message);
    check_error_message("Failed to delete the task list");
    this->sql_status = STATUS_SUCCESS;
}

/**
 * @brief  This renames a task table in the database
 * @note   This also sets the status code
 * @param  oldname: the old (current) task table name
 * @param  newname: the new task table name
 * @retval None
 */
void SQL::SQLEngine::rename_task_table(std::string oldname, std::string newname)
{
    // check if the old table name is legal
    if (!isValidTableName(oldname))
    {
        // set error and return
        this->sql_status = STATUS_INVALID_TABLE_NAME;
        return;
    }
    // check if old table exists
    if (!isStringInsideVector(get_task_table_names(), oldname))
    {
        // set error and return
        this->sql_status = STATUS_TABLE_NOT_EXIST;
        return;
    }
    // check if the new table name is legal
    if (!isValidTableName(newname))
    {
        // set error and return
        this->sql_status = STATUS_INVALID_TABLE_NAME;
        return;
    }
    // check if the new table exists
    if (isStringInsideVector(get_task_table_names(), newname))
    {
        // set error and return
        this->sql_status = STATUS_TABLE_ALREADY_EXISTS;
        return;
    }
    // get the table id
    std::string id = get_task_table_id_from_name(oldname);
    // make command string
    std::string cmd = "UPDATE ATLAS SET NAME='" + newname + "' WHERE TABLE_ID='" + id + "';";
    // rename the table
    this->exit_status = sqlite3_exec(this->database, cmd.c_str(), NULL, 0, &this->message);
    check_error_message("Failed to rename the task list");
    this->sql_status = STATUS_SUCCESS;
}

/**
 * @brief  This changes the column labels of a task table
 * @note   This also sets the status code
 * @param  column: the column to edit
 * @param  label: the new label
 * @param  name: the name of the task table
 * @retval None
 */
void SQL::SQLEngine::edit_task_table(int column, std::string label, std::string name)
{
    // store column string
    std::string colName;
    // get column to update
    switch (column)
    {
    case SQL::COLUMN_NAME:
        colName = "TASK_LABEL";
        break;
    case SQL::COLUMN_LOCATION:
        colName = "LOCATION_LABEL";
        break;
    case SQL::COLUMN_DATE:
        colName = "DATE_LABEL";
        break;
    case SQL::COLUMN_DONE:
        colName = "DONE_LABEL";
        break;
    default:
        // return to prevent damage
        this->sql_status = STATUS_INVALID_COLUMN_SELECTOR;
        return;
    }
    // check if column label is valid
    if (!isValidColumnLabel(label))
    {
        this->sql_status = STATUS_INVALID_COLUMN_NAME;
        return;
    }
    // check if table name is valid
    if (!isValidTableName(name))
    {
        this->sql_status = STATUS_INVALID_TABLE_NAME;
        return;
    }
    // check if table is real
    if (!isStringInsideVector(get_task_table_names(), name))
    {
        this->sql_status = STATUS_TABLE_NOT_EXIST;
        return;
    }
    // execute command
    std::string cmd = "UPDATE ATLAS SET " + colName + "='" + label + "' WHERE NAME='" + name + "';";
    this->exit_status = sqlite3_exec(this->database, cmd.c_str(), NULL, 0, &this->message);
    check_error_message("Failed to edit label on task list");
    this->sql_status = STATUS_SUCCESS;
}

/**
 * @brief  This gets the ATLAS entry for a task table
 * @note   This also sets the status code
 * @param  name: the task table name
 * @retval the ATLAS entry for the requested task table
 */
atlas_t SQL::SQLEngine::get_task_table_info(std::string name)
{
    // make atlas struct
    atlas_t target = {"", "", "", "", "", ""};
    // check if the table name is legal
    if (!isValidTableName(name))
    {
        // set error and return
        this->sql_status = STATUS_INVALID_TABLE_NAME;
        return target;
    }
    // check if table exists
    if (!isStringInsideVector(get_task_table_names(), name))
    {
        // set error and return
        this->sql_status = STATUS_TABLE_NOT_EXIST;
        return target;
    }
    // make command string
    std::string cmd = "SELECT * FROM ATLAS WHERE NAME=\"" + name + "\";";
    // execute command
    this->exit_status = sqlite3_exec(this->database, cmd.c_str(), SQL::callback_get_task_table_info, &target, &this->message);
    check_error_message("Failed to delete the task list");
    // set status
    this->sql_status = STATUS_SUCCESS;
    // return information
    return target;
}

/**
 * @brief  This gets a list of tasks from a task table
 * @note   This also sets the status code
 * @param  name: the name of the task table
 * @retval a list of tasks
 */
std::vector<task_t> SQL::SQLEngine::get_tasks_from_task_table(std::string name)
{
    // make container for results
    std::vector<task_t> results;
    // check if table name is valid
    if (!isValidTableName(name))
    {
        this->sql_status = STATUS_INVALID_TABLE_NAME;
        return results;
    }
    // check if table exists
    if (!isStringInsideVector(get_task_table_names(), name))
    {
        this->sql_status = STATUS_TABLE_NOT_EXIST;
        return results;
    }
    // get id for table
    std::string id = get_task_table_id_from_name(name);
    // make SQL command
    std::string cmd = "SELECT NAME,LOCATION,strftime(\"%s\",DATE),DONE,HASH FROM \"" + id + "\" ORDER BY DONE ASC, DATE ASC;";
    // execute command
    this->exit_status = sqlite3_exec(this->database, cmd.c_str(), callback_get_tasks_from_task_table, &results, &this->message);
    check_error_message("Failed to edit label on task list");
    // return results
    this->sql_status = STATUS_SUCCESS;
    return results;
}

/**
 * @brief  This adds a task to a task table
 * @note   This also sets the status code
 * @param  table: the table name
 * @param  task: the name of the task
 * @param  location: the location of the task
 * @param  date: the timestamp of the task
 * @param  done: the state of the task
 * @retval None
 */
void SQL::SQLEngine::add_task(std::string table, std::string task, std::string location, long date, bool done)
{
    // check if table is valid
    if (!isValidTableName(table))
    {
        this->sql_status = STATUS_INVALID_TABLE_NAME;
        return;
    }
    // check if table exists
    if (!isStringInsideVector(get_task_table_names(), table))
    {
        this->sql_status = STATUS_TABLE_NOT_EXIST;
        return;
    }
    // check if task is valid
    if (!isValidTaskName(task))
    {
        this->sql_status = STATUS_INVALID_TASK_NAME;
        return;
    }
    // check if location is valid
    if (!isValidLocationName(location))
    {
        this->sql_status = STATUS_INVALID_TASK_LOCATION;
        return;
    }
    // check if date is valid
    if (!isValidDateLong(date))
    {
        this->sql_status = STATUS_INVALID_TASK_DATE;
        return;
    }
    // check if task already exists
    if (isTaskInsideVector(get_tasks_from_task_table(table), {task, location, "", date, 0}))
    {
        // task exists so do nothing
        this->sql_status = STATUS_TASK_ALREADY_EXISTS;
        return;
    }
    // generate task id
    std::string hash = generate_id();
    // get table id
    std::string table_id = get_task_table_id_from_name(table);
    // make SQL command
    std::string cmd = "INSERT INTO \"" + table_id + "\" VALUES (\"" + task + "\",\"" + location + "\",datetime(\"" + std::to_string(date) + "\", 'unixepoch'),\"" + std::to_string(done) + "\",\"" + hash + "\");";
    // execute command
    this->exit_status = sqlite3_exec(this->database, cmd.c_str(), NULL, 0, &this->message);
    // check for errors
    check_error_message("Failed to add task to task list");
    // set status code
    this->sql_status = STATUS_SUCCESS;
}

/**
 * @brief  This deletes a task from a task table
 * @param  table: the task table name
 * @param  hash: the task hash
 * @retval None
 */
void SQL::SQLEngine::delete_task(std::string table, std::string hash)
{
    // check if table is valid
    if (!isValidTableName(table))
    {
        this->sql_status = STATUS_INVALID_TABLE_NAME;
        return;
    }
    // check if table exists
    if (!isStringInsideVector(get_task_table_names(), table))
    {
        this->sql_status = STATUS_TABLE_NOT_EXIST;
        return;
    }
    // check if hash is inside table
    if (!isTaskHashInVector(get_tasks_from_task_table(table), hash))
    {
        this->sql_status = STATUS_HASH_NOT_EXIST;
        return;
    }
    // get task table id
    std::string table_id = get_task_table_id_from_name(table);
    // make SQL command
    std::string cmd = "DELETE FROM \"" + table_id + "\" WHERE HASH=\"" + hash + "\";";
    // execute command
    this->exit_status = sqlite3_exec(this->database, cmd.c_str(), NULL, 0, &this->message);
    // check for errors
    check_error_message("Failed to delete task from task list");
}

/**
 * @brief  This edits a task from a task table
 * @param  table: the name of the task table
 * @param  hash: the hash of the task
 * @param  column: the column to edit
 * @param  value: the new value
 * @retval None
 */
void SQL::SQLEngine::edit_task(std::string table, std::string hash, int column, std::string value)
{
    // check if table is valid
    if (!isValidTableName(table))
    {
        this->sql_status = STATUS_INVALID_TABLE_NAME;
        return;
    }
    // check if table exists
    if (!isStringInsideVector(get_task_table_names(), table))
    {
        this->sql_status = STATUS_TABLE_NOT_EXIST;
        return;
    }
    // check if task hash is real
    if (!isTaskHashInVector(get_tasks_from_task_table(table), hash))
    {
        // the hash does not exist
        this->sql_status = STATUS_HASH_NOT_EXIST;
        return;
    }
    // get table id
    std::string table_id = get_task_table_id_from_name(table);
    // store column string
    std::string colName;
    // store new value string
    std::string newVal;
    // get column to update
    switch (column)
    {
    case SQL::COLUMN_NAME:
        // set needed vars
        colName = "NAME";
        newVal = "\"" + value + "\"";
        // check if name is valid
        if (!isValidTaskName(value))
        {
            this->sql_status = STATUS_INVALID_TASK_NAME;
            return;
        }
        break;
    case SQL::COLUMN_LOCATION:
        // set needed vars
        colName = "LOCATION";
        newVal = "\"" + value + "\"";
        // check if location is valid
        if (!isValidLocationName(value))
        {
            // the task location is invalid
            this->sql_status = STATUS_INVALID_TASK_LOCATION;
            return;
        }
        break;
    case SQL::COLUMN_DATE:
        // set needed vars
        colName = "DATE";
        // check if it is a valid date string
        if (!isValidDateString(value))
        {
            sql_status = STATUS_INVALID_TASK_DATE;
            return;
        }
        // convert value to an integer string
        value = std::to_string(dateStringToLong(value));
        // check that long is within range
        if (!isValidDateLong(atol(value.c_str())))
        {
            sql_status = STATUS_INVALID_TASK_DATE;
            return;
        }
        // generate SQL string
        newVal = "datetime(\"" + value + "\", 'unixepoch')";
        break;
    case SQL::COLUMN_DONE:
        // set needed vars
        colName = "DONE";
        newVal = "\"" + doneStringToSQL(value) + "\"";
        // check if done status is valid
        if (!isValidDoneString(value))
        {
            this->sql_status = STATUS_INVALID_TASK_DONE;
            return;
        }
        break;
    default:
        // return to prevent damage
        this->sql_status = STATUS_INVALID_COLUMN_SELECTOR;
        return;
    }
    // execute command
    std::string cmd = "UPDATE \"" + table_id + "\" SET " + colName + "=" + newVal + " WHERE HASH=\"" + hash + "\";";
    this->exit_status = sqlite3_exec(this->database, cmd.c_str(), NULL, 0, &this->message);
    check_error_message("Failed to edit task entry");
    this->sql_status = STATUS_SUCCESS;
}

/**
 * @brief  This generates a hash
 * @retval a unique hash
 */
std::string SQL::SQLEngine::generate_id()
{
    long time = std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
    std::string hash_num = std::to_string(time);
    std::string hash_alpha = "";
    for (char c : hash_num)
    {
        hash_alpha += c + 17;
    }
    return hash_alpha;
}

/**
 * @brief  This checks of there was a SQL error and crashes if needed
 * @param  prefix: a string to append to the SQL error
 * @retval None
 */
void SQL::SQLEngine::check_error_code(std::string prefix)
{
    if (this->exit_status != SQLITE_OK)
    {
        crash(prefix + ": " + (std::string)sqlite3_errmsg(this->database));
    }
}

/**
 * @brief  This checks of there was a SQL error and crashes if needed
 * @param  prefix: a string explaining why the error occurred
 * @retval None
 */
void SQL::SQLEngine::check_error_message(std::string reason)
{
    if (this->exit_status != SQLITE_OK)
    {
        crash(reason + ": " + this->message);
    }
}

/**
 * @brief  This opens a SQLite database file
 * @retval None
 */
void SQL::SQLEngine::open_database()
{
    // check if file path is valid
    if (!isValidFilePath(this->filepath))
    {
        crash("File path is not valid");
    }
    // attempt to open database
    this->exit_status = sqlite3_open(this->filepath.c_str(), &this->database);
    // check for errors
    check_error_code("Failed to open database");
}

/**
 * @brief  This closes a SQLite database file and frees allocated memory
 * @retval None
 */
void SQL::SQLEngine::close_database()
{
    // close the database
    sqlite3_close(this->database);
    // free strings
    sqlite3_free(this->message);
}

/**
 * @brief  This generates the table of task tables if it doesn't exist
 * @retval None
 */
void SQL::SQLEngine::generate_atlas()
{
    // set SQL query
    std::string str = "CREATE TABLE IF NOT EXISTS ATLAS ("
                      "NAME           STRING NOT NULL,"
                      "TASK_LABEL     STRING NOT NULL,"
                      "LOCATION_LABEL STRING NOT NULL,"
                      "DATE_LABEL     STRING NOT NULL,"
                      "DONE_LABEL     STRING NOT NULL,"
                      "TABLE_ID       STRING NOT NULL);";
    // execute command
    this->exit_status = sqlite3_exec(this->database, str.c_str(), NULL, 0, &this->message);
    // check for errors
    check_error_message("Failed to create table");
}
