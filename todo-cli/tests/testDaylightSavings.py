# To-Do CLI
# Copyright (C) 2022 Cameron Himes
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import subprocess

CMD_COMMON = "./todo-cli --database foo.db "
FAILED = 0;


def placeTable(dateStr):
    """
    Place a task in a new database and return the task list.

    Args:
        dateStr (str): A date string in the form "YYYY:MM:DD hh:mm:ss"

    Returns:
        str: the output of the --tasks flag
    """
    subprocess.getoutput(CMD_COMMON + "--add-table foo")
    subprocess.getoutput(CMD_COMMON + "--add-task foo tname tloc \"{0}\" 0".format(dateStr))
    out = subprocess.getoutput(CMD_COMMON + "--tasks foo")
    subprocess.getoutput(CMD_COMMON + "--delete-table foo")
    return out


def grabDate(out):
    """
    This returns the date string from todo-cli's --tasks command.

    Args:
        out (str): the output from todo-cli's --tasks command

    Returns:
        str: a date in the format YYYY:MM:DD hh:mm:ss
    """
    start = out.index("2022")
    end = start + 19
    return out[start:end]


def checkMatch(dateStr):
    """
    Inserts a task into the database and checks if the date has been altered

    Args:
        dateStr (str): a date in the format YYYY:MM:DD hh:mm:ss
    """
    global FAILED
    outRaw = placeTable(dateStr);
    outDate = grabDate(outRaw);
    if outDate != dateStr:
        print("FAILED:", outDate, "!=", dateStr)
        FAILED += 1;
    else:
        print("PASSED:", outDate, "==", dateStr)


def testTimesOnDate(date):
    """
    Tests task creation for all times of a day

    Args:
        date (str): a date in the format YYYY:MM:DD
    """
    for h in range(24):
        for m in [0, 1, 59]:
            for s in [0, 1, 59]:
                dateStr = "{date} {hh}:{mm}:{ss}".format(
                    date=date,
                    hh=str(h).zfill(2),
                    mm=str(m).zfill(2),
                    ss=str(s).zfill(2)
                )
                checkMatch(dateStr)


def main():
    """
    Run tests for all months in the year 2022
    """
    for month in range(1, 13):
        date = "2022-{mm}-01".format(mm=str(month).zfill(2))
        try:
            testTimesOnDate(date);
        except KeyboardInterrupt:
            break
    if FAILED:
        print("Failed", FAILED, "tests.")
    else:
        print("All tests passed.")


if __name__ == "__main__":
    main();
