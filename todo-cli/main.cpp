/*
 * To-Do CLI
 * Copyright (C) 2022 Cameron Himes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include "main.hpp"
#include "args.hpp"
#include "driver.hpp"

/**
 * @brief  This is the main method for the program
 * @param  argc: number of arguments
 * @param  argv: array of arguments
 * @retval exit code
 */
int main(int argc, char **argv)
{
    // parse arguments
    command_data_t cmd = ARGS::parseArgs(argc, argv);
    // execute the command
    Driver::run(cmd);
    // exit program
    return 0;
}
