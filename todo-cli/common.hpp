/*
 * To-Do CLI
 * Copyright (C) 2022 Cameron Himes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

// imports
#include <string>
#include <vector>

// struct for task info
#ifndef TASK_T
#define TASK_T
typedef struct task
{
    std::string name;
    std::string location;
    std::string hash;
    long date;
    bool done;
} task_t;
#endif

// struct for table information
#ifndef ATLAS_T
#define ATLAS_T
typedef struct atlas
{
    std::string name;
    std::string task_label;
    std::string location_label;
    std::string date_label;
    std::string done_label;
    std::string table_id;
} atlas_t;
#endif

// a struct to store command information
#ifndef CMD_T
#define CMD_T
typedef struct command_data
{
    std::vector<std::string> args;
    std::string database;
    int command;
    bool api;
} command_data_t;
#endif

// functions
void crash(std::string message);
bool stringEquals(std::string a, std::string b);
bool isStringInsideVector(std::vector<std::string> vector, std::string string);
bool isTaskInsideVector(std::vector<task_t> vector, task_t task);
bool isTaskHashInVector(std::vector<task_t> vector, std::string hash);
long dateStringToLong(std::string dateStr);
std::string dateLongToString(long dateInt);
bool isValidTableName(std::string table);
bool isValidColumnLabel(std::string label);
bool isValidTaskName(std::string name);
bool isValidLocationName(std::string location);
bool isValidDateString(std::string date);
bool isValidDateLong(long date);
bool isValidDoneString(std::string done);
bool isValidFilePath(std::string path);
std::string doneStringToSQL(std::string done);
bool isNumber(char character);
bool isNumber(std::string number);
bool isLeapYear(int year);
int getDaysInMonth(int year, int month);
std::string fixFilePath(std::string path);
std::string getHome();
std::string leftJustify(std::string text, long unsigned int size);
