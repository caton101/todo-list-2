/*
 * To-Do CLI
 * Copyright (C) 2022 Cameron Himes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

// usage information
#define PROGRAM_USAGE "Usage: todo-cli [--api] [--database FILEPATH] <COMMAND> <ARGS>"
// description of the program
#define PROGRAM_DESCRIPTION "todo-cli -- a simple to-do list application"
// version of the program
#define PROGRAM_VERSION "To-Do CLI 3.0"
// padding for task list
#define TASKS_PADDING 3
// the label for the task hash
#define HASH_LABEL "ID"
// default database file path
#define DATABASE_PATH "~/.todolist2.db"
// default values
#define DEFAULT_NAME_LABEL "NAME"
#define DEFAULT_LOCATION_LABEL "LOCATION"
#define DEFAULT_DATE_LABEL "DATE"
#define DEFAULT_DONE_LABEL "DONE"
