/*
 * To-Do CLI
 * Copyright (C) 2022 Cameron Himes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

// only define SQL once
#ifndef SQL_HPP
#define SQL_HPP

// imports
#include <sqlite3.h>
#include <vector>
#include <string>
#include "common.hpp"

// put everything inside SQL namespace
namespace SQL
{
    // internal flags
    const int COLUMN_NAME = 0;
    const int COLUMN_LOCATION = 1;
    const int COLUMN_DATE = 2;
    const int COLUMN_DONE = 3;

    // status codes
    const int STATUS_SUCCESS = 0;
    const int STATUS_INVALID_TABLE_NAME = 1;
    const int STATUS_INVALID_TASK_NAME = 2;
    const int STATUS_INVALID_TASK_LOCATION = 3;
    const int STATUS_INVALID_TASK_DATE = 4;
    const int STATUS_INVALID_TASK_DONE = 5;
    const int STATUS_INVALID_COLUMN_SELECTOR = 6;
    const int STATUS_INVALID_COLUMN_NAME = 7;
    const int STATUS_TABLE_NOT_EXIST = 8;
    const int STATUS_TABLE_ALREADY_EXISTS = 9;
    const int STATUS_TASK_ALREADY_EXISTS = 10;
    const int STATUS_HASH_NOT_EXIST = 11;

    // callbacks
    int callback_get_task_table_names(void *data, int cols, char **colValues, char **colNames);
    int callback_get_task_table_id_from_name(void *data, int cols, char **colValues, char **colNames);
    int callback_get_tasks_from_task_table(void *data, int cols, char **colValues, char **colNames);
    int callback_get_task_table_info(void *data, int cols, char **colValues, char **colNames);

    // misc SQL function
    std::string explain_status(int status);

    // SQLEngine class
    class SQLEngine
    {
    public:
        // methods
        SQLEngine(std::string filepath);
        ~SQLEngine();
        int get_status();
        std::vector<std::string> get_task_table_names();
        std::string get_task_table_id_from_name(std::string name);
        void add_task_table(std::string name);
        void delete_task_table(std::string name);
        void rename_task_table(std::string oldname, std::string newname);
        atlas_t get_task_table_info(std::string name);
        void edit_task_table(int column, std::string label, std::string name);
        std::vector<task_t> get_tasks_from_task_table(std::string name);
        void add_task(std::string table, std::string task, std::string location, long date, bool done);
        void delete_task(std::string table, std::string hash);
        void edit_task(std::string table, std::string hash, int column, std::string value);

    private:
        // fields
        std::string filepath;
        char *message;
        sqlite3 *database;
        int exit_status = 0;
        int sql_status = STATUS_SUCCESS;

        // methods
        std::string generate_id();
        void check_error_code(std::string prefix);
        void check_error_message(std::string reason);
        void open_database();
        void close_database();
        void generate_atlas();
    };
}

// end SQL
#endif
