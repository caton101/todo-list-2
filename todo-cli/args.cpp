/*
 * To-Do CLI
 * Copyright (C) 2022 Cameron Himes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

// include statements
#include <vector>
#include <string>
#include <iostream>
#include "args.hpp"
#include "common.hpp"
#include "config.hpp"

/**
 * @brief  This checks if the parsing offset is valid.
 * @param  argc: the argument count passed to main
 * @param  offset: the current parsing offset
 * @retval true if the offset is inside the argument count
 */
bool ARGS::offsetLegal(int argc, int offset)
{
    return offset < argc;
}

/**
 * @brief  This checks if there are enough arguments for parsing.
 * @param  argc: the argument count passed to main
 * @param  offset: the current parsing offset
 * @param  args: the number of arguments needed
 * @retval true if there are enough arguments for parsing
 */
bool ARGS::notEnoughArgs(int argc, int offset, int args)
{
    return argc - (offset + 1) < args;
}

/**
 * @brief  This converts a command to a string
 * @param  command: the COMMAND_* definition
 * @retval a string representing the command name
 */
std::string ARGS::commandToString(int command)
{
    switch (command)
    {
    case COMMAND_TABLES:
        return "COMMAND_TABLES";
    case COMMAND_EDIT_LABEL:
        return "COMMAND_EDIT_LABEL";
    case COMMAND_DELETE_TABLE:
        return "COMMAND_DELETE_TABLE";
    case COMMAND_RENAME_TABLE:
        return "COMMAND_RENAME_TABLE";
    case COMMAND_ADD_TABLE:
        return "COMMAND_ADD_TABLE";
    case COMMAND_TASKS:
        return "COMMAND_TASKS";
    case COMMAND_EDIT_TASK:
        return "COMMAND_EDIT_TASK";
    case COMMAND_ADD_TASK:
        return "COMMAND_ADD_TASK";
    case COMMAND_DELETE_TASK:
        return "COMMAND_DELETE_TASK";
    case COMMAND_HELP:
        return "COMMAND_HELP";
    case COMMAND_USAGE:
        return "COMMAND_USAGE";
    case COMMAND_VERSION:
        return "COMMAND_VERSION";
    case COMMAND_LICENSE:
        return "COMMAND_LICENSE";
    case COMMAND_TEST:
        return "COMMAND_TEST";
    case COMMAND_ERROR_NO_ARGS:
        return "COMMAND_ERROR_NO_ARGS";
    case COMMAND_ERROR_FEW_ARGS:
        return "COMMAND_ERROR_FEW_ARGS";
    case COMMAND_ERROR_EXCESS_ARGS:
        return "COMMAND_ERROR_EXCESS_ARGS";
    case COMMAND_ERROR_INVALID:
        return "COMMAND_ERROR_INVALID";
    default:
        return "LOOKUP_FAILED";
    }
}

/**
 * @brief  This parses the arguments passed to main.
 * @param  argc: the number of arguments
 * @param  argv: the array of arguments
 * @retval a command struct representing the arguments
 */
command_data_t ARGS::parseArgs(int argc, char **argv)
{
    // make container for command information
    command_data_t response;
    // set default command to return invalid error
    response.args.clear();
    response.database = fixFilePath(DATABASE_PATH);
    response.command = COMMAND_ERROR_INVALID;
    response.api = false;
    // make an offset for scanning arguments
    int offset = 1;
    // check if there are not enough arguments
    if (argc < 2)
    {
        // return error for no arguments
        response.command = COMMAND_ERROR_NO_ARGS;
        return response;
    }
    // check if api mode is enabled
    if (stringEquals(argv[offset], "--api"))
    {
        // enable api mode
        response.api = true;
        // increment offset
        offset++;
    }

    // check if api was the only argument
    if (!offsetLegal(argc, offset))
    {
        response.command = COMMAND_ERROR_NO_ARGS;
    }

    // check if test mode is enabled
    if (stringEquals(argv[1], "--test"))
    {
        // set command type
        response.command = COMMAND_TEST;
        // add rest of arguments to the args
        for (int i = 2; i < argc; i++)
        {
            response.args.push_back(argv[i]);
        }
        // return the response
        return response;
    }

    // check if database was provided
    if (offsetLegal(argc, offset) && stringEquals(argv[offset], "--database"))
    {
        // check if there are enough arguments left
        if (notEnoughArgs(argc, offset, 1))
        {
            // can not process request
            response.command = COMMAND_ERROR_FEW_ARGS;
            return response;
        }
        // read the database file path
        response.database = fixFilePath(argv[offset + 1]);
        // increment offset
        offset += 2;
    }

    // get a command
    if (offsetLegal(argc, offset))
    {
        // check tables command
        if (stringEquals(argv[offset], "--tables"))
        {
            // set up response
            response.command = COMMAND_TABLES;
            // increment offset
            offset++;
        }
        // check edit-label command
        else if (stringEquals(argv[offset], "--edit-label"))
        {
            // check if there are enough arguments left
            if (notEnoughArgs(argc, offset, 3))
            {
                // can not process request
                response.command = COMMAND_ERROR_FEW_ARGS;
                return response;
            }
            // set up response
            response.command = COMMAND_EDIT_LABEL;
            response.args.push_back(argv[offset + 1]);
            response.args.push_back(argv[offset + 2]);
            response.args.push_back(argv[offset + 3]);
            // increment offset
            offset += 4;
        }
        // check delete-table command
        else if (stringEquals(argv[offset], "--delete-table"))
        {
            // check if there are enough arguments left
            if (notEnoughArgs(argc, offset, 1))
            {
                // can not process request
                response.command = COMMAND_ERROR_FEW_ARGS;
                return response;
            }
            // set up response
            response.command = COMMAND_DELETE_TABLE;
            response.args.push_back(argv[offset + 1]);
            // increment offset
            offset += 2;
        }
        // check rename-table command
        else if (stringEquals(argv[offset], "--rename-table"))
        {
            // check if there are enough arguments left
            if (notEnoughArgs(argc, offset, 2))
            {
                // can not process request
                response.command = COMMAND_ERROR_FEW_ARGS;
                return response;
            }
            // set up response
            response.command = COMMAND_RENAME_TABLE;
            response.args.push_back(argv[offset + 1]);
            response.args.push_back(argv[offset + 2]);
            // increment offset
            offset += 3;
        }
        // check add-table command
        else if (stringEquals(argv[offset], "--add-table"))
        {
            // check if there are enough arguments left
            if (notEnoughArgs(argc, offset, 1))
            {
                // can not process request
                response.command = COMMAND_ERROR_FEW_ARGS;
                return response;
            }
            // set up response
            response.command = COMMAND_ADD_TABLE;
            response.args.push_back(argv[offset + 1]);
            // increment offset
            offset += 2;
        }
        // check tasks command
        else if (stringEquals(argv[offset], "--tasks"))
        {
            // check if there are enough arguments left
            if (notEnoughArgs(argc, offset, 1))
            {
                // can not process request
                response.command = COMMAND_ERROR_FEW_ARGS;
                return response;
            }
            // set up response
            response.command = COMMAND_TASKS;
            response.args.push_back(argv[offset + 1]);
            // increment offset
            offset += 2;
        }
        // check edit-task command
        else if (stringEquals(argv[offset], "--edit-task"))
        {
            // check if there are enough arguments left
            if (notEnoughArgs(argc, offset, 4))
            {
                // can not process request
                response.command = COMMAND_ERROR_FEW_ARGS;
                return response;
            }
            // set up response
            response.command = COMMAND_EDIT_TASK;
            response.args.push_back(argv[offset + 1]);
            response.args.push_back(argv[offset + 2]);
            response.args.push_back(argv[offset + 3]);
            response.args.push_back(argv[offset + 4]);
            // increment offset
            offset += 5;
        }
        // check add-task command
        else if (stringEquals(argv[offset], "--add-task"))
        {
            // check if there are enough arguments left
            if (notEnoughArgs(argc, offset, 5))
            {
                // can not process request
                response.command = COMMAND_ERROR_FEW_ARGS;
                return response;
            }
            // set up response
            response.command = COMMAND_ADD_TASK;
            response.args.push_back(argv[offset + 1]);
            response.args.push_back(argv[offset + 2]);
            response.args.push_back(argv[offset + 3]);
            response.args.push_back(argv[offset + 4]);
            response.args.push_back(argv[offset + 5]);
            // increment offset
            offset += 6;
        }
        // check delete-task command
        else if (stringEquals(argv[offset], "--delete-task"))
        {
            // check if there are enough arguments left
            if (notEnoughArgs(argc, offset, 2))
            {
                // can not process request
                response.command = COMMAND_ERROR_FEW_ARGS;
                return response;
            }
            // set up response
            response.command = COMMAND_DELETE_TASK;
            response.args.push_back(argv[offset + 1]);
            response.args.push_back(argv[offset + 2]);
            // increment offset
            offset += 3;
        }
        // check help command
        else if (stringEquals(argv[offset], "--help"))
        {
            // set up response
            response.command = COMMAND_HELP;
            // increment offset
            offset++;
        }
        // check usage command
        else if (stringEquals(argv[offset], "--usage"))
        {
            // set up response
            response.command = COMMAND_USAGE;
            // increment offset
            offset++;
        }
        // check version command
        else if (stringEquals(argv[offset], "--version"))
        {
            // set up response
            response.command = COMMAND_VERSION;
            // increment offset
            offset++;
        }
        // check license command
        else if (stringEquals(argv[offset], "--license"))
        {
            // set up response
            response.command = COMMAND_LICENSE;
            // increment offset
            offset++;
        }
        // command is invalid
        else
        {
            // set up response
            response.command = COMMAND_ERROR_INVALID;
        }
    }

    // check if there were extra arguments
    if (argc != offset && response.command != COMMAND_ERROR_INVALID)
    {
        // change response to an error
        response.args.clear();
        response.command = COMMAND_ERROR_EXCESS_ARGS;
    }

    // return the command information
    return response;
}
