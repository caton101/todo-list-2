/*
 * To-Do CLI
 * Copyright (C) 2022 Cameron Himes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

// imports
#include <string>
#include <vector>
#include "common.hpp"
#include "sql.hpp"

// put everything inside driver namespace
namespace Driver
{
    // functions
    void list_tables(command_data_t command);
    void edit_table(command_data_t command);
    void delete_table(command_data_t command);
    void rename_table(command_data_t command);
    void add_table(command_data_t command);
    void list_tasks(command_data_t command);
    void edit_task(command_data_t command);
    void add_task(command_data_t command);
    void delete_task(command_data_t command);
    void show_help(command_data_t command);
    void show_usage(command_data_t command);
    void show_version(command_data_t command);
    void show_license(command_data_t command);
    void internal_test(command_data_t command);
    void error_no_args(command_data_t command);
    void error_few_args(command_data_t command);
    void error_excess_args(command_data_t command);
    void error_invalid(command_data_t command);
    void error_database_file(command_data_t command);
    void run(command_data_t command);
    void generic_error(command_data_t command, std::string error);
    std::string fix_dates(SQL::SQLEngine *sql, std::string table, std::string name, std::string location, std::string date, bool done, int *status, std::string *explanation);
}
