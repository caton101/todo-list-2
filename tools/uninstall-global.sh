#!/bin/sh

# settings
CLI_EXEC_FILE=todo-cli
TUI_EXEC_FILE=todo-tui
CLI_MAN_FILE=todo-cli.1.gz
TUI_MAN_FILE=todo-tui.1.gz
BIN_DIRECTORY=/usr/local/bin
MAN_PATH=/usr/local/share/man/man1

# get sudo
sudo -v

# delete installed files
sudo rm -f $BIN_DIRECTORY/$CLI_EXEC_FILE
sudo rm -f $BIN_DIRECTORY/$TUI_EXEC_FILE
sudo rm -f $MAN_PATH/$CLI_MAN_FILE
sudo rm -f $MAN_PATH/$TUI_MAN_FILE

# refresh manpages
sudo mandb
