#!/bin/sh

# settings
CLI_EXEC_FILE=todo-cli
TUI_EXEC_FILE=todo-tui
CLI_MAN_FILE=todo-cli.1.gz
TUI_MAN_FILE=todo-tui.1.gz
BIN_DIRECTORY=/usr/local/bin
MAN_PATH=/usr/local/share/man/man1
MANDB_COMMAND=mandb
SUDO_COMMAND=sudo

# runtime vars
COPY_CLI_EXEC=1
COPY_TUI_EXEC=1
COPY_CLI_MAN=1
COPY_TUI_MAN=1
REFRESH_MAN=1

# check that build files exist
if [ ! -f "./build/todo-cli" ]
then
    echo "The todo-cli executable does not exist. Did you forget to build?"
    exit 1
fi

if [ ! -f "./build/todo-tui" ]
then
    echo "The todo-tui executable does not exist. Did you forget to build?"
    exit 1
fi

if [ ! -f "./build/todo-cli.1.gz" ]
then
    echo "The todo-cli manpage does not exist. Did you forget to build?"
    exit 1
fi

if [ ! -f "./build/todo-tui.1.gz" ]
then
    echo "The todo-tui manpage does not exist. Did you forget to build?"
    exit 1
fi

# check if sudo command exists
if [ "$USER" = "root" ]
then
    echo "Running as root. Ignoring privilege escalation command."
    SUDO_COMMAND=""
elif [ ! -f "$(which $SUDO_COMMAND 2>/dev/null)" ]
then
    echo "Could not find privilege escalation command. Refusing to run."
    exit 1
fi

# check if install path is clear
if [ -f "$BIN_DIRECTORY/$CLI_EXEC_FILE" ]
then
    printf "The todo-cli executable is already installed. Do you want to overwrite it (y/n)? "
    read ans
    if [ "${ans^^}" = "Y" ]
    then
        COPY_CLI_EXEC=1
    elif [ "${ans^^}" = "N" ]
    then
        COPY_CLI_EXEC=0
    else
        echo "Invalid response. Aborting installation."
        exit 1
    fi
fi

if [ -f "$BIN_DIRECTORY/$TUI_EXEC_FILE" ]
then
    printf "The todo-tui executable is already installed. Do you want to overwrite it (y/n)? "
    read ans
    if [ "${ans^^}" = "Y" ]
    then
        COPY_TUI_EXEC=1
    elif [ "${ans^^}" = "N" ]
    then
        COPY_TUI_EXEC=0
    else
        echo "Invalid response. Aborting installation."
        exit 1
    fi
fi

if [ -f "$MAN_PATH/$CLI_MAN_FILE" ]
then
    printf "The todo-cli manpage is already installed. Do you want to overwrite it (y/n)? "
    read ans
    if [ "${ans^^}" = "Y" ]
    then
        COPY_CLI_MAN=1
    elif [ "${ans^^}" = "N" ]
    then
        COPY_CLI_MAN=0
    else
        echo "Invalid response. Aborting installation."
        exit 1
    fi
fi

if [ -f "$MAN_PATH/$TUI_MAN_FILE" ]
then
    printf "The todo-tui manpage is already installed. Do you want to overwrite it (y/n)? "
    read ans
    if [ "${ans^^}" = "Y" ]
    then
        COPY_TUI_MAN=1
    elif [ "${ans^^}" = "N" ]
    then
        COPY_TUI_MAN=0
    else
        echo "Invalid response. Aborting installation."
        exit 1
    fi
fi

# check if man can be refreshed
if [ -f "$(which $MANDB_COMMAND 2>/dev/null)" ]
then
    echo "Manpage database will be updated."
    REFRESH_MAN=1
else
    echo "Manpage database will not be refreshed. Please do this manually."
    REFRESH_MAN=0
fi

# ensure directories exist
$SUDO_COMMAND mkdir -p $BIN_DIRECTORY
$SUDO_COMMAND mkdir -p $MAN_PATH

# perform copying
if [ $COPY_CLI_EXEC -eq 1 ]
then
    echo "Copying todo-cli executable..."
    $SUDO_COMMAND cp todo-cli/todo-cli $BIN_DIRECTORY/$CLI_EXEC_FILE
else
    echo "Skipping todo-cli executable."
fi

if [ $COPY_TUI_EXEC -eq 1 ]
then
    echo "Copying todo-tui executable..."
    $SUDO_COMMAND cp todo-tui/todo-tui $BIN_DIRECTORY/$TUI_EXEC_FILE
else
    echo "Skipping todo-tui executable."
fi

if [ $COPY_CLI_MAN -eq 1 ]
then
    echo "Copying todo-cli manpage."
    $SUDO_COMMAND cp man/todo-cli.1.gz $MAN_PATH/$CLI_MAN_FILE
else
    echo "Skipping todo-cli manpage."
fi

if [ $COPY_TUI_MAN -eq 1 ]
then
    echo "Copying todo-tui manpage."
    $SUDO_COMMAND cp man/todo-tui.1.gz $MAN_PATH/$TUI_MAN_FILE
else
    echo "Skipping todo-tui manpage."
fi

# refresh manpage
if [ $REFRESH_MAN -eq 1 ]
then
    echo "Updating manpage database..."
    $SUDO_COMMAND $MANDB_COMMAND
else
    echo "Not updating manpages."
fi

