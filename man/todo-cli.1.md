% TODO-CLI(1) todo-cli 3.0
% Cameron Himes
% April 2023

# NAME

todo-cli \-\- a simple to-do list application

# SYNOPSIS

todo-cli \<COMMAND\>\
todo-cli [\-\-api] \<COMMAND\>\
todo-cli [\-\-database FILE] \<COMMAND\>\
todo-cli [\-\-api] [\-\-database FILE] \<COMMAND\>

# DESCRIPTION

This program tracks your tasks and deadlines. Unlike alternatives, this program
is designed for use in a terminal. It is lightweight and offers great
flexibility. All tasks are kept in a single database file for easy
synchronization with a third party tool like Nextcloud or Dropbox. It includes
features such as timezone adjustments, multiple lists, and changing the field
labels. It also contains an API mode (called with todo-cli \-\-api \<command\>)
which will convert output to an easy to parse JSON object.

# OPTIONS

\-\-api
: print output as a JSON object

\-\-database FILE
: use a custom database path

\-\-tables
: list all tables

\-\-edit-label TABLE SELECTOR LABEL
: edit a label for a table

\-\-delete-table TABLE
: delete a table

\-\-rename-table TABLE NAME
: rename a table

\-\-add-table NAME
: add a table

\-\-tasks TABLE
: list all tasks in a table

\-\-edit-task TABLE TASK SELECTOR VALUE
: edit a task attribute

\-\-add-task TABLE NAME LOCATION DATE DONE
: add a task

\-\-delete-task TABLE TASK
: delete a task

\-\-help
: show help information

\-\-license
: show license information

\-\-usage
: show command syntax

\-\-version
: show version number

# EXAMPLES

todo-cli \-\-tables
: List the available task tables

todo-cli \-\-add-table foo
: Add a task table called foo

todo-cli \-\-delete-table foo
: Delete a task table called foo

todo-cli \-\-rename-table foo bar
: Rename a task table called foo to bar

todo-cli \-\-edit-label foo NAME myLabel
: Change the task name label to myLabel for the foo task table

todo-cli \-\-tasks foo
: List the task inside the foo task table

todo-cli \-\-add-task foo "Finish manpage" "GitLab" "2021-12-26 23:05:00" false
: Add a task called "Finish manpage" to the foo task table

todo-cli \-\-delete-task foo id
: Delete a task with the specified id from the foo task table

todo-cli \-\-edit-task foo id LOCATION home
: Change the location of a task inside the foo table with the specified id to home

todo-cli \-\-version
: Display program version

todo-cli \-\-usage
: Display usage information

todo-cli \-\-help
: Display all commands and information

todo-cli \-\-license
: Display license information

# EXIT VALUES

0
: Success

1
: Error occurred

# CAVEATS

The [\-\-api] and/or [\-\-database FILE] flag must come before the actual
command. If using both, the [\-\-api] flag must come before the [\-\-database
FILE] flag.

# SOURCE CODE

https://gitlab.com/caton101/todo-list-2

# COPYRIGHT

To-Do CLI

Copyright (C) 2022 Cameron Himes

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <http://www.gnu.org/licenses/>.
