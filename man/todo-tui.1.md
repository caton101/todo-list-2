% TODO-TUI(1) todo-tui 3.0
% Cameron Himes
% April 2023

# NAME

todo-tui \-\- a simple to-do list application in a TUI

# SYNOPSIS

todo-tui \<FILE\>

# DESCRIPTION

This program is a simple TUI To-Do List which uses todo-cli(1)'s API. Everything
that can be done in the command line tool can be done from this application.

# EXAMPLES

todo-tui
: Use the default task database

todo-tui FILE
: Use FILE as the task database

# KEYBINDS

## Global

UP
: Move selection up

DOWN
: Move selection down

y
: Answers yes to a prompt

n
: Answers no to a prompt

## Table Selection Menu

a
: Add a table

d
: Delete selected table

e
: Edit selected table

ENTER
: Show selected table

DEL
: Delete selected table

ESC
: Close program

## Task List Menu

a
: Add a task

d
: Delete selected task

e
: Edit selected task

ENTER
: Edit selected task

DEL
: Delete selected task

TAB
: Toggle completion of selected task

ESC
: Return to table menu

# EXIT VALUES

0
: Success

1
: Error occurred

# CAVEATS

This program relies on todo-cli(1) being installed and inside $PATH.

# SOURCE CODE

https://gitlab.com/caton101/todo-list-2

# CREDITS

Most of the codebase belongs to Cameron Himes which is under the GNU General
Public License Version 3. The few exceptions are mentioned below.

JSON was parsed using JSON by Niels Lohmann which is available under the MIT
License.

https://github.com/nlohmann/json

Calls to todo-cli are performed using cpp-subprocess by Arun Muralidharan which
is available under the MIT License.

https://github.com/arun11299/cpp-subprocess

Code to enter and exit raw mode is provided by Kilo using this guide by
Salvatore Sanfilippo which is under the BSD 2-Clause "Simplified" License.

https://github.com/snaptoken/kilo-src

https://viewsourcecode.org/snaptoken/kilo/02.enteringRawMode.html

# COPYRIGHT

To-Do TUI

Copyright (C) 2023 Cameron Himes

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <http://www.gnu.org/licenses/>.
